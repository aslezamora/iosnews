//
//  NewsDetailViewController.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/13/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailViewController : UIViewController <UIScrollViewDelegate, UIWebViewDelegate, UITextViewDelegate>

@property (nonatomic,strong) NSArray *arrNewsContent, *arrAllNews;
@property (nonatomic,assign) NSInteger index;

@end
