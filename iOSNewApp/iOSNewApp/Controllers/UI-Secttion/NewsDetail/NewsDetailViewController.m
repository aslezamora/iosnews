//
//  NewsDetailViewController.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/13/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "MBProgressHUD.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "SaveID.h"
#import "DMActivityInstagram.h"
#import "TMTumblrActivity.h"
#import "CommentsViewController.h"
#import "Constant.h"

@interface NewsDetailViewController ()

@property (nonatomic, strong) IBOutlet UIWebView *webNews;
@property (nonatomic, strong) NSString *strTitle,*strURL,*strCompany,*strDate;
@property (nonatomic, strong) UIBarButtonItem *webButtonItem, *chatButtonItem, *saveButtonItem, *shareButtonItem;
@property (nonatomic, assign) BOOL isJsonView, isFavorited;
@property (nonatomic, strong) NSString *strWebContent, *strJsonContent, *strHTMLButton, *strHTMLHead, *strTitleBody;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollNews;
@property (nonatomic, strong) IBOutlet UIButton *btnPrev, *btnNext;
@property (nonatomic, strong) MBProgressHUD* hud;
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic, strong) NSManagedObjectContext *context;

@end

@implementation NewsDetailViewController
@synthesize arrNewsContent, arrAllNews, index;

- (void)viewDidLoad
{

    [super viewDidLoad];
    self.isJsonView = YES;
    self.isFavorited = NO;

    if ([[arrAllNews valueForKey:@"content"] objectAtIndex:index] != [NSNull null])
        self.strJsonContent = [[arrAllNews valueForKey:@"content"] objectAtIndex:index];
    else
        self.strJsonContent = @"";
    
    if ([[arrAllNews valueForKey:@"url"] objectAtIndex:index] != [NSNull null])
        self.strWebContent = [[arrAllNews valueForKey:@"url"] objectAtIndex:index];
    else
        self.strWebContent = @"";
    
    //Coredata
    self.appDelegate = [[UIApplication sharedApplication] delegate];
    self.context = [self.appDelegate managedObjectContext];
 
    //Navigation Bar
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-back.png"]
                                                                         style:UIBarButtonItemStyleBordered target:self action:@selector(backToMain:)];
    
    self.webButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-web.png"]
                                                          style:UIBarButtonItemStyleBordered target:self action:@selector(webItem:)];
    
    self.chatButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-chat.png"]
                                                                       style:UIBarButtonItemStyleBordered target:self action:@selector(chatItem:)];
    
    self.shareButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-share.png"]
                                                                       style:UIBarButtonItemStyleBordered target:self action:@selector(shareItem:)];
    
    //Check if Favorited
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SaveID"
                                          inManagedObjectContext:self.context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [self.context executeFetchRequest:fetchRequest error:&error];
    for (SaveID *saveID in fetchedObjects) {
        if ([saveID.newsID intValue] == [[[self.arrAllNews valueForKey:@"id"] objectAtIndex:index] intValue]) {
            self.isFavorited = YES;
            self.saveButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"favorited.png"]
                                                                   style:UIBarButtonItemStyleBordered target:self action:@selector(unFavoritedItem:)];
            self.saveButtonItem.tintColor = [UIColor colorWithRed:93/255.0 green:193/255.0 blue:240/255.0 alpha:1.0];

            break;
        }else{
            self.isFavorited = NO;
            self.saveButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-favorite-1.png"]
                                                                   style:UIBarButtonItemStyleBordered target:self action:@selector(favoritedItem:)];
        }
    }
    if (fetchedObjects.count == 0) {
        self.saveButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-favorite-1.png"]
                                                               style:UIBarButtonItemStyleBordered target:self action:@selector(favoritedItem:)];
    }
    
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    self.navigationItem.rightBarButtonItems = @[self.shareButtonItem,self.saveButtonItem,self.chatButtonItem,self.webButtonItem];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:146/255.0 green:201/255.0 blue:197/255.0 alpha:1.0];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //Setup Webview Title Body
    [self setupTitleBody];
    
    //WebView
    self.webNews.delegate = self;
    self.webNews.backgroundColor = [UIColor whiteColor];
    [self.webNews clearsContextBeforeDrawing];
    [self.webNews loadHTMLString:[NSString stringWithFormat:@"%@ %@ %@ %@", self.strHTMLHead,self.strTitleBody,self.strJsonContent, self.strHTMLButton] baseURL:nil];
    self.webNews.scrollView.delegate = self;
    
    //More Button
    self.btnPrev.layer.borderWidth = 1.0f;
    self.btnPrev.layer.borderColor = [[UIColor grayColor] CGColor];
    [self.btnPrev addTarget:self action:@selector(prevNews:) forControlEvents:UIControlEventTouchUpInside];
    
    self.btnNext.layer.borderWidth = 1.0f;
    self.btnNext.layer.borderColor = [[UIColor grayColor] CGColor];
    [self.btnNext addTarget:self action:@selector(nextNews:) forControlEvents:UIControlEventTouchUpInside];

    if (index == 0) {
        [self.btnPrev setEnabled:NO];
        [self.btnNext setEnabled:YES];
    }else if (index == arrAllNews.count - 1){
        [self.btnPrev setEnabled:YES];
        [self.btnNext setEnabled:NO];
    }
    else{
        [self.btnPrev setEnabled:YES];
        [self.btnNext setEnabled:YES];
    }
}

-(void)setupTitleBody{
    //Title
    if ([[arrAllNews valueForKey:@"title"] objectAtIndex:index] != [NSNull null])
        self.strTitle = [[arrAllNews valueForKey:@"title"] objectAtIndex:index];
    else
        self.strTitle = @"";
    
    //URL
    if ([[arrAllNews valueForKey:@"url"] objectAtIndex:index] != [NSNull null])
        self.strURL = [[arrAllNews valueForKey:@"url"] objectAtIndex:index];
    else
        self.strURL = @"";
    
    //Company
    if ([[arrAllNews valueForKey:@"company"] objectAtIndex:index] != [NSNull null])
        self.strCompany = [[arrAllNews valueForKey:@"company"] objectAtIndex:index];
    else
        self.strCompany = @"";
    
    //Date
    if ([[arrAllNews valueForKey:@"created_at"] objectAtIndex:index] != [NSNull null]){
        NSString *dateInString = [[arrAllNews valueForKey:@"created_at"] objectAtIndex:index];
        NSString *yearString = [dateInString substringWithRange:NSMakeRange(0, 4)];
        NSString *monthString = [dateInString substringWithRange:NSMakeRange(5, 2)];
        NSString *dayString = [dateInString substringWithRange:NSMakeRange(8, 2)];
        monthString = [Utility getMonthShort:monthString];
        self.strDate = [NSString stringWithFormat:@"%@ %@, %@",monthString,dayString,yearString];
    }
    else
        self.strDate = @"";
    
    //View on Source Button
    self.strHTMLButton = @"<div align=\"center\" id=\"main-wrapper\">"
                          @"<a href='news' class=\"news\">"
                           @"<button type=\"button\" style=\" -moz-border-radius: 1px; -webkit-border-radius: 1px; border-radius: 1px; width: 150px;height: 40px; align:center; font-weight: bold; font-size: 12px;\">View on Source</button>"
                          @"</a>"
                         @"</div>";
    
    //Content Head max-height: 10%;
    self.strHTMLHead = @"<head>"
                        @"<style type=\"text/css\">"
                         @"img{ top:0; bottom:10; left:10; right:10; margin:auto; max-width:100%; height:200px;}"
                         @"iframe{ top:0; bottom:10; left:10; right:10; margin:auto; max-width:100%; height:200px;}"
                         @"h1{ font-family:Lato-Bold; font-size:16px;}"
                         @"h2{ font-family:Lato-Bold; font-size:16px;}"
                         @"h3{ font-family:Lato-Bold; font-size:18px;}"
                         @"p{ font-family:Lato-Regular; font-size:14px;}"
                         @".title{ font-family:Lato-Bold; font-size:14px;}"
                         @".url{ font-family:Lato-Regular; font-size: 10px; }"
                         @".date{ font-family:Lato-Regular; font-size: 10px; }"
                        @"</style>"
                       @"</head>";
    
    //Content Body Title
    self.strTitleBody = [NSString stringWithFormat:
                         @"<div>"
                         @"<div class=\"title\">%@</div>"
                         @"<a class=\"url\" href=\"%@\" target=\"_blank\">%@</a>"
                         @"<div class=\"date\">%@</div>"
                         @"</div>",self.strTitle,self.strURL,self.strCompany,self.strDate];
}

-(void)setJsonImage{
    if (self.isJsonView == YES) {
        self.webButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-web.png"]
                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(webItem:)];
    }else{
        self.webButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-json.png"]
                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(jsonItem:)];
    }
    self.navigationItem.rightBarButtonItems = @[self.shareButtonItem,self.saveButtonItem,self.chatButtonItem,self.webButtonItem];
    
}

-(void)setSaveImage{
    if (self.isFavorited == YES) {
        self.saveButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"favorited.png"]
                                                               style:UIBarButtonItemStyleBordered target:self action:@selector(unFavoritedItem:)];
        self.saveButtonItem.tintColor = [UIColor colorWithRed:93/255.0 green:193/255.0 blue:240/255.0 alpha:1.0];
        [self saveIdToDB];
    }else{
        self.saveButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-favorite-1.png"]
                                                               style:UIBarButtonItemStyleBordered target:self action:@selector(favoritedItem:)];
        
        [self deleteIdFromDB];
    }
    self.navigationItem.rightBarButtonItems = @[self.shareButtonItem,self.saveButtonItem,self.chatButtonItem,self.webButtonItem];
}

-(void)saveIdToDB{
    //Fetch Data
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SaveID"
                                              inManagedObjectContext:self.context];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"newsID = %@", [[self.arrAllNews valueForKey:@"id"] objectAtIndex:index]]];
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:nil];
    if ([results count] == 0)
    {
        SaveID *saveEntity = [NSEntityDescription
                              insertNewObjectForEntityForName:@"SaveID"
                              inManagedObjectContext:self.context];
        saveEntity.newsID = [[self.arrAllNews valueForKey:@"id"] objectAtIndex:index];
        saveEntity.saveDate = [NSDate date];
    }
    
    NSError *err, *error;
    if (![self.context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [err localizedDescription]);
    }
}

-(void)deleteIdFromDB{
    //Fetch Data
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SaveID"
                                              inManagedObjectContext:self.context];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"newsID = %@", [[self.arrAllNews valueForKey:@"id"] objectAtIndex:index]]];
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:nil];
    for (NSManagedObject *object in results) {
        [self.context deleteObject:object];
    }
    
    NSError *err, *error;
    if (![self.context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [err localizedDescription]);
    }
}

#pragma mark - Device Orientation
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.view reloadInputViews];
}


#pragma mark - Buttons
- (IBAction)backToMain:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)jsonItem:(id)sender
{
    self.strJsonContent = [[arrAllNews valueForKey:@"content"] objectAtIndex:index];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //self.webNews.frame = CGRectMake(0, 137, 320, 431);
    self.isJsonView = YES;
    
    //Setup Webview Title Body
    [self setupTitleBody];
    
    [self.webNews loadHTMLString:[NSString stringWithFormat:@"%@ %@ %@ %@", self.strHTMLHead, self.strTitleBody, self.strJsonContent, self.strHTMLButton] baseURL:nil];
    [self setJsonImage];
    [self.btnPrev setHidden:YES];
    [self.btnNext setHidden:YES];
}

- (IBAction)webItem:(id)sender
{
    self.strWebContent = [[arrAllNews valueForKey:@"url"] objectAtIndex:index];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.webNews.frame = self.view.bounds;
    self.isJsonView = NO;
    NSURL *newsUrl = [NSURL URLWithString:self.strWebContent];
    NSURLRequest *request = [NSURLRequest requestWithURL:newsUrl];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [self.webNews clearsContextBeforeDrawing];
    [self.webNews loadRequest:request];
    [self setJsonImage];
    [self.btnPrev setHidden:YES];
    [self.btnNext setHidden:YES];
}

- (IBAction)chatItem:(id)sender
{
    CommentsViewController *commentsVC = [[CommentsViewController alloc]initWithNibName:@"CommentsViewController" bundle:nil];
    [commentsVC setFacebookURL:@"http://facebook.com"];
    [self.navigationController pushViewController:commentsVC animated:YES];
}

- (IBAction)unFavoritedItem:(id)sender
{
    self.isFavorited = NO;
    [self setSaveImage];
}

- (IBAction)shareItem:(id)sender
{
    DMActivityInstagram *instagramActivity = [[DMActivityInstagram alloc] init];
    TMTumblrActivity *tumblrActivity = [[TMTumblrActivity alloc] init];
    
    NSString *message = [[self.arrAllNews valueForKey:@"title"] objectAtIndex:index];
    NSURL *url= [[NSURL alloc]initWithString:[[self.arrAllNews valueForKey:@"url"] objectAtIndex:index]];
    NSArray *shareItems = @[message,url];
    
//    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
//                                   UIActivityTypePrint,
//                                   UIActivityTypeAssignToContact,
//                                   UIActivityTypeSaveToCameraRoll,
//                                   UIActivityTypeAddToReadingList,
//                                   UIActivityTypePostToFlickr,
//                                   UIActivityTypePostToVimeo,
//                                   UIActivityTypeCopyToPasteboard];
    
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:shareItems applicationActivities:@[instagramActivity,tumblrActivity]];
    //activityVC.excludedActivityTypes = excludeActivities;
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)favoritedItem:(id)sender
{
    self.isFavorited = YES;
    [self setSaveImage];
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"" message:@"Saved" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alert1 show];
    [self performSelector:@selector(dismiss:) withObject:alert1 afterDelay:0.5];
}

- (IBAction)prevNews:(id)sender
{
    index = index-1;
    if (self.isJsonView == YES) {
        self.strJsonContent = [[arrAllNews valueForKey:@"content"] objectAtIndex:index];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        //Setup Webview Title Body
        [self setupTitleBody];
        
        [self.webNews loadHTMLString:[NSString stringWithFormat:@"%@ %@ %@ %@", self.strHTMLHead, self.strTitleBody, self.strJsonContent, self.strHTMLButton] baseURL:nil];
        [self setJsonImage];
    }else{
        self.strWebContent = [[arrAllNews valueForKey:@"url"] objectAtIndex:index];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.webNews.frame = self.view.bounds;
        NSURL *newsUrl = [NSURL URLWithString:self.strWebContent];
        NSURLRequest *request = [NSURLRequest requestWithURL:newsUrl];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        [self.webNews clearsContextBeforeDrawing];
        [self.webNews loadRequest:request];
        [self setJsonImage];
    }
    [self.btnPrev setHidden:YES];
    [self.btnNext setHidden:YES];
}

- (IBAction)nextNews:(id)sender
{
    index = index+1;
    if (self.isJsonView == YES) {
        self.strJsonContent = [[arrAllNews valueForKey:@"content"] objectAtIndex:index];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        //Setup Webview Title Body
        [self setupTitleBody];
        
        [self.webNews loadHTMLString:[NSString stringWithFormat:@"%@ %@ %@ %@", self.strHTMLHead, self.strTitleBody, self.strJsonContent, self.strHTMLButton] baseURL:nil];
        [self setJsonImage];
    }else{
        self.strWebContent = [[arrAllNews valueForKey:@"url"] objectAtIndex:index];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.webNews.frame = self.view.bounds;
        NSURL *newsUrl = [NSURL URLWithString:self.strWebContent];
        NSURLRequest *request = [NSURLRequest requestWithURL:newsUrl];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        [self.webNews clearsContextBeforeDrawing];
        [self.webNews loadRequest:request];
        [self setJsonImage];
    }
    [self.btnPrev setHidden:YES];
    [self.btnNext setHidden:YES];
}


#pragma mark - Alert
-(void)dismiss:(UIAlertView*)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark - Scrollview
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.contentOffset.y
       >=
       (scrollView.contentSize.height - scrollView.frame.size.height)){
        [self.btnPrev setHidden:NO];
        [self.btnNext setHidden:NO];
        if (index == 0) {
            [self.btnPrev setEnabled:NO];
            [self.btnNext setEnabled:YES];
        }else if (index == arrAllNews.count - 1){
            [self.btnPrev setEnabled:YES];
            [self.btnNext setEnabled:NO];
        }
        else{
            [self.btnPrev setEnabled:YES];
            [self.btnNext setEnabled:YES];
        }
        
    }
    else{
        [self.btnPrev setHidden:YES];
        [self.btnNext setHidden:YES];
    }
    return;
}

#pragma mark - TextView
-(BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
    
    //Go To Website
    self.strWebContent = [[arrAllNews valueForKey:@"url"] objectAtIndex:index];
    self.webNews.frame = self.view.bounds;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.isJsonView = NO;
    NSURL *newsUrl = [NSURL URLWithString:self.strWebContent];
    NSURLRequest *request = [NSURLRequest requestWithURL:newsUrl];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [self.webNews clearsContextBeforeDrawing];
    [self.webNews loadRequest:request];
    [self setJsonImage];
    [self.btnPrev setHidden:YES];
    [self.btnNext setHidden:YES];
    
    return NO;
}

#pragma mark - WebView
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSRange UrlInRequestRange = [[request.URL.absoluteString lowercaseString] rangeOfString:[@"news" lowercaseString]];
    if(navigationType == UIWebViewNavigationTypeLinkClicked && UrlInRequestRange.location != NSNotFound)
    {
        self.strWebContent = [[arrAllNews valueForKey:@"url"] objectAtIndex:index];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.webNews.frame = self.view.bounds;
        self.isJsonView = NO;
        NSURL *newsUrl = [NSURL URLWithString:self.strWebContent];
        NSURLRequest *request = [NSURLRequest requestWithURL:newsUrl];
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        [self.webNews clearsContextBeforeDrawing];
        [self.webNews loadRequest:request];
        [self setJsonImage];
        [self.btnPrev setHidden:YES];
        [self.btnNext setHidden:YES];
        return NO;
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webview{

    // adjust the content size of webview scrollview
    NSString *output = [self.webNews stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"main-wrapper\").offsetHeight;"];
    
    self.webNews.scrollView.contentSize = CGSizeMake(self.webNews.frame.size.width, self.webNews.scrollView.contentSize.height + [output intValue] + self.btnNext.frame.size.height + 30);

    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
