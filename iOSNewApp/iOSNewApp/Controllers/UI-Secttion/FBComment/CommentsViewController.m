//
//  CommentsViewController.m
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 7/1/14.
//  Copyright (c) 2014 SurrealFacts. All rights reserved.
//

#import "CommentsViewController.h"

@interface CommentsViewController ()

@end

@implementation CommentsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupView];
    [self setupFBView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Main
- (void)setupView
{
    //Add background image to navigation bar.
//    UINavigationBar *navBar = self.navigationController.navigationBar;
//    UIImage *image = [UIImage imageNamed:@"topbar.png"];
//    [navBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-back.png"]
                                                                       style:UIBarButtonItemStyleBordered target:self action:@selector(backToMain:)];
    self.navigationItem.leftBarButtonItem = backButtonItem;
    self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    self.navigationItem.title = @"Comments";
    
    //Set our Menu Button
    UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnClose setFrame:CGRectMake(8.0, 7.0, 30.0, 30.0)];
    [btnClose setImage:[UIImage imageNamed:@"secondview_cross.png"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(dismissCommentsView) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightSpacer = [[UIBarButtonItem alloc]
                                    initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                    target:nil action:nil];
    rightSpacer.width = -10;
    UIBarButtonItem *btnCloseNavBar =[[UIBarButtonItem alloc] initWithCustomView:btnClose];
    self.navigationItem.rightBarButtonItems = [NSArray
                                               arrayWithObjects:rightSpacer, btnCloseNavBar, nil];
    
    //Set our navigation title bar font
//    UIFont *GoboldFont = [UIFont fontWithName:@"Gobold Bold" size:21.0f];
//    NSMutableDictionary *titleBarAttributes = [NSMutableDictionary dictionaryWithDictionary: [[UINavigationBar appearance] titleTextAttributes]];
//    [titleBarAttributes setValue:GoboldFont forKey:NSFontAttributeName];
//    [self.navigationController.navigationBar setTitleTextAttributes:titleBarAttributes];
//    [self.navigationController.navigationBar.topItem setTitle:@"Comments"];
}

- (void)setupFBView
{
    [self loadHtmlWidget];
}

#pragma mark - Buttons
- (IBAction)backToMain:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) dismissCommentsView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)loadHtmlWidget
{
	_rootWebView.backgroundColor = [UIColor clearColor];
    NSString *finalHTML;
    NSString *detailUrl = [self facebookURL];
	NSString *imagePath = [[NSBundle mainBundle] resourcePath];
	imagePath = [imagePath stringByReplacingOccurrencesOfString:@"/" withString:@"//"];
	imagePath = [imagePath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSError *error = nil;
    NSString *html = [[NSString alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"fb-comments" ofType:@"html"] encoding:NSUTF8StringEncoding error:&error];
    finalHTML = [html stringByReplacingOccurrencesOfString:@"PAGEURL" withString:detailUrl];
	//[html stringByReplacingOccurrencesOfString:@"PAGEURL" withString:detailUrl options:NSCaseInsensitiveSearch range: NSMakeRange(0, [html length])];
	finalHTML = [finalHTML stringByReplacingOccurrencesOfString:@"PAGEWIDTH" withString:[NSString stringWithFormat:@"%f",_rootWebView.bounds.size.width-15]];
	[_rootWebView loadHTMLString:finalHTML baseURL:[NSURL URLWithString: detailUrl]];
    //[_activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
	if (aWebView == _rootWebView) {
        //[_activityIndicator stopAnimating];
	}else {
		[aWebView stringByEvaluatingJavaScriptFromString:@"window.close = function() {  window.location = \"close://close\"; }"];
	}
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if(webView == _rootWebView)
    {
//        [_activityIndicator stopAnimating];
//        [self dismissCommentsView];
    }
}

-(BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
navigationType:(UIWebViewNavigationType)navigationType;
{
	if (webView == _rootWebView) {
		NSRange FBloginRequest = [[[request URL] absoluteString]
                                  rangeOfString:@"/login.php"];
		
		if (FBloginRequest.location != NSNotFound) {
			[self loadAndPresentPopupWithRequest:request];
			return NO;
		}
		
		NSRange loggedInBackUrlRange = [[[request URL] absoluteString]
                                        rangeOfString:@"/plugins/login_success.php?refsrc"];
		
		if (loggedInBackUrlRange.location != NSNotFound) {
			[self loadHtmlWidget];
			return NO;
		}
	} else {
		NSRange loggedInBackUrlRange = [[[request URL] absoluteString]
                                        rangeOfString:@"/plugins/login_success.php"];
		if (loggedInBackUrlRange.location != NSNotFound) {
			[_rootWebView loadRequest:request];
			[self closePopup];
			return NO;
		}
		
	}
	return navigationType != UIWebViewNavigationTypeLinkClicked;
}

- (void) loadAndPresentPopupWithRequest:(NSURLRequest *)request
{
	UIViewController *popupView = [[UIViewController alloc] init];
	popupView.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
	popupView.view.backgroundColor = [UIColor yellowColor];
	
	UIWebView *popupWebView = [[UIWebView alloc] initWithFrame:popupView.view.bounds];
	popupWebView.delegate = self;
	[popupView.view addSubview:popupWebView];
	[popupWebView loadRequest:request];
    
	UINavigationController *popupNavController = [[UINavigationController alloc] initWithRootViewController:popupView];
	popupNavController.modalPresentationStyle = UIModalPresentationFormSheet;
	
	popupView.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                                   initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                   target:self
                                                   action:@selector(closePopup)];
	[self presentViewController:popupNavController animated:YES completion:nil];
}


-(void)closePopup
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (NSString*) decodeFromPercentEscapeString:(NSString *) string {
    return (__bridge NSString *) CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                         (__bridge CFStringRef) string,
                                                                                         CFSTR(""),
                                                                                         kCFStringEncodingUTF8);
}
@end
