//
//  CommentsViewController.h
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 7/1/14.
//  Copyright (c) 2014 SurrealFacts. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsViewController : UIViewController<UIWebViewDelegate>
{

}
@property (nonatomic, strong) NSString *facebookURL;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIWebView *rootWebView;
@end
