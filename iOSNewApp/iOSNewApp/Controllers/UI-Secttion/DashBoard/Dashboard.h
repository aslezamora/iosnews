//
//  Dashboard.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/10/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HHPanningTableViewCell.h"
#import "SFViewController.h"

@interface Dashboard : SFViewController <UITableViewDelegate, UITableViewDataSource, UISearchDisplayDelegate, UISearchBarDelegate, HHPanningTableViewCellDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, strong) NSString *selectedCategory;

@end
