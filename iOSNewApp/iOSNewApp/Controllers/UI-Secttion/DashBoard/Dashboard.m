//
//  Dashboard.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/10/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "Dashboard.h"
#import "SideMenuTableViewController.h"
#import "SWRevealViewController.h"
#import "HHPanningTableViewCell.h"
#import "SideMenuCell.h"
#import "SearchViewController.h"
#import "DashboardData.h"
#import "MBProgressHUD.h"
#import "NewsDetailViewController.h"
#import "AppDelegate.h"
#import "News.h"
#import "SaveID.h"
#import "DMActivityInstagram.h"
#import "TMTumblrActivity.h"
#import "DashboardIpadCell.h"
#import "Reachability.h"
#import "SearchViewController.h"
#import "SearchCell.h"
#import "Utility.h"
#import "UIImageView+AFNetworking.h"

@interface Dashboard ()

@property (nonatomic, strong) IBOutlet UITableView *tbDashboard;
@property (nonatomic, strong) IBOutlet UICollectionView *cvDashboard;
@property (nonatomic, strong) IBOutlet UILabel *noNews;
@property (nonatomic, strong) NSMutableArray *arrAllNews, *arrSearchResults, *arrNews;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (assign, nonatomic) BOOL isFavorited, isFiltered;
//@property (nonatomic, strong) UISearchDisplayController *displayController;
@property (nonatomic, strong) MBProgressHUD* hud;
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSString *strTitle, *strURL;
@property (nonatomic, strong) NSMutableString *strCategories;
@property (nonatomic, strong) UIView *grayView;
@property (nonatomic, strong) UISearchDisplayController *searchController;

@end

@implementation Dashboard
@synthesize selectedIndex, selectedCategory;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isFavorited = NO;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //CoreData
    self.appDelegate = [[UIApplication sharedApplication] delegate];
    self.context = [self.appDelegate managedObjectContext];
    
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)  {
        //CollectionView
        self.cvDashboard.dataSource = self;
        self.cvDashboard.delegate = self;
        
        self.cvDashboard.autoresizingMask = UIViewAutoresizingFlexibleHeight |
        UIViewAutoresizingFlexibleWidth;
        [self.cvDashboard registerNib:[UINib nibWithNibName:@"DashboardCell" bundle:nil] forCellWithReuseIdentifier:@"DashboardCell"];
    } else {
        //TableView
        self.tbDashboard.dataSource = self;
        self.tbDashboard.delegate = self;
        self.tbDashboard.frame = self.view.bounds;
    }
    
    //SideMenu
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-menu.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    UIBarButtonItem *searchButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-search.png"]
                                                                         style:UIBarButtonItemStyleBordered target:self action:@selector(revealSearch:)];
    
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:146/255.0 green:201/255.0 blue:197/255.0 alpha:1.0];
    self.navigationItem.rightBarButtonItem = searchButtonItem;
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithRed:146/255.0 green:201/255.0 blue:197/255.0 alpha:1.0];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, self.view.bounds.size.width, 196);
    //[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];;
    [self.navigationItem.backBarButtonItem setImage:[UIImage imageNamed:@"button-back.png"]];
    if (selectedIndex == 0) {
        self.navigationItem.title = @"Top Stories";
    }else{
        self.navigationItem.title = selectedCategory;
    }
    
    
    //Pull to Refresh
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0];
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(updateDatabase:) forControlEvents:UIControlEventValueChanged];
    [self.tbDashboard addSubview:self.refreshControl];
    
    //Notification Observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getAllNewsSuccess:)
                                                 name:@"allNewsSuccess"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(share:)
                                                 name:@"shareClicked"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(favorited:)
                                                 name:@"favoriteClicked"
                                               object:nil];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"News"
                                                  inManagedObjectContext:self.context];
        [fetchRequest setEntity:entity];
        NSError *error;
        NSArray *fetchedObjects = [self.context executeFetchRequest:fetchRequest error:&error];
        
        NSMutableArray *allNews = [[NSMutableArray alloc]init];
        self.arrAllNews = [[NSMutableArray alloc]init];
        for (News *news in fetchedObjects) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setValue:news.newsID forKey:@"id"];
            [dict setValue:news.title forKey:@"title"];
            [dict setValue:news.url forKey:@"url"];
            [dict setValue:news.company forKey:@"company"];
            [dict setValue:news.comment forKey:@"comment"];
            [dict setValue:news.mainImage forKey:@"mainImage"];
            [dict setValue:news.category forKey:@"category"];
            [dict setValue:news.content forKey:@"content"];
            [dict setValue:news.postedDate forKey:@"created_at"];
            [dict setValue:news.vote forKey:@"votes"];
            [allNews addObject:dict];
        }
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES];
        [allNews sortUsingDescriptors:[NSArray arrayWithObject:sort]];
        
        //Filter News by Category
        if (selectedIndex == 0) {
            for (id item in allNews) {
                [self.arrAllNews addObject:item];
            }
        }else{
            for (id item in allNews) {
                NSArray *categories = [[item valueForKey:@"category"] componentsSeparatedByString:@","];
                
                for (NSString *num in categories) {
                    NSString *pureNumbers = [[num componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
                    
                    if (selectedIndex == [pureNumbers intValue]) {
                        [self.arrAllNews addObject:item];
                        break;
                    }
                }
            }
        }
        
        //Check if No Available News
        if (self.arrAllNews.count == 0) {
            [self.noNews setHidden:NO];
        }else{
            [self.noNews setHidden:YES];
        }
        
        //Copy contents of arrAllNews
        self.arrNews = [[NSMutableArray alloc]init];
        self.arrNews = [self.arrAllNews copy];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    } else {
        [[DashboardData sharedInstance] getAllNews];
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound){
        [self.cvDashboard reloadData];
    }
    else
        [self.tbDashboard reloadData];
}

#pragma mark - Device Orientation
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    self.cvDashboard.frame = self.view.bounds;
    [self.cvDashboard reloadData];
}

#pragma mark - Search Bar
- (IBAction)revealSearch:(id)sender
{
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,20, self.view.frame.size.width,40)];
    searchBar.placeholder = @"Search";
    searchBar.backgroundColor = [UIColor grayColor];
    
    [self.navigationController.view addSubview:searchBar];
    
    self.searchController = [[UISearchDisplayController alloc]initWithSearchBar:searchBar contentsController:self];
    self.searchController.delegate = self;
    self.searchController.searchResultsDataSource = self;
    self.searchController.searchResultsDelegate = self;
    searchBar.delegate = self;
    searchBar.showsCancelButton = YES;
    [searchBar becomeFirstResponder];
    [self.searchDisplayController.searchBar sizeToFit];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar removeFromSuperview];
    searchBar.text = nil;
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    if(searchString.length == 0)
    {
        self.isFiltered = FALSE;
    }
    else
    {
        self.isFiltered = true;
        self.arrSearchResults = [[NSMutableArray alloc] init];
        
        for (id item in self.arrAllNews)
        {
            NSRange nameRange = [[item valueForKey:@"title"] rangeOfString:searchString options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound)
            {
                [self.arrSearchResults addObject:item];
            }
        }
    }
    
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    
    if(searchBar.text.length == 0)
    {
        self.isFiltered = FALSE;
    }
    else
    {
        self.isFiltered = true;
        self.arrSearchResults = [[NSMutableArray alloc] init];
        
        for (id item in self.arrAllNews)
        {
            NSRange nameRange = [[item valueForKey:@"title"] rangeOfString:searchBar.text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound)
            {
                [self.arrSearchResults addObject:item];
            }
        }
    }
    
    
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound){
        SearchViewController *searchVC = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
        searchVC.strSearchKeyword = searchBar.text;
        searchVC.arrSearch = self.arrSearchResults;
        [self.navigationController pushViewController:searchVC animated:YES];
    }
    else
        [self.tbDashboard reloadData];
    
    //Copy contents of arrAllNews
    self.arrNews = [[NSMutableArray alloc]init];
    self.arrNews = [self.arrAllNews copy];
}

-(void)searchDisplayController:(UISearchDisplayController *)controller didShowSearchResultsTableView:(UITableView *)tableView{
    
    tableView.frame = CGRectMake(self.tbDashboard.frame.origin.x, self.tbDashboard.frame.origin.y+40, self.tbDashboard.frame.size.width, self.tbDashboard.frame.size.height-40);
    
//    //Check if iPad
//    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
//    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)
//        [tableView setHidden:YES];
    
}

#pragma mark - Full to Refresh
- (void)updateDatabase:(id)sender{
    //Notification Observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getAllNewsSuccess:)
                                                 name:@"allNewsSuccess"
                                               object:nil];
    //Update Data
    [[DashboardData sharedInstance] getAllNews];
    [self performSelector:@selector(updateTable:) withObject:nil afterDelay:1];
}

- (void)updateTable:(id)sender
{
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)
        [self.cvDashboard reloadData];
    else
        [self.tbDashboard reloadData];
    
    [self.refreshControl endRefreshing];
}

#pragma mark - Notification Buttons
- (void)favorited:(NSNotification*)notification
{
    NSLog(@"%@",notification.object);
    NSInteger index = [[notification.object valueForKey:@"index"] integerValue];
    //Check if Favorited
    NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SaveID"
                                              inManagedObjectContext:self.context];
    [fetchRequest setEntity:entity];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"newsID = %@", [[self.arrAllNews valueForKey:@"id"] objectAtIndex:index]]];
    NSArray *results = [self.context executeFetchRequest:fetchRequest error:nil];
    if ([results count] == 0)
    {
        SaveID *saveEntity = [NSEntityDescription
                              insertNewObjectForEntityForName:@"SaveID"
                              inManagedObjectContext:self.context];
        saveEntity.newsID = [[self.arrAllNews valueForKey:@"id"] objectAtIndex:index];
        saveEntity.saveDate = [NSDate date];

        UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"" message:@"Saved" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
        [alert1 show];
        [self performSelector:@selector(dismiss:) withObject:alert1 afterDelay:0.5];
        
        //Notification to change the image in panning
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:[NSNumber numberWithBool:YES] forKey:@"isFavorited"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"isFavorited" object:dict];
        
        HHPanningTableViewCell *cell = [[HHPanningTableViewCell alloc]init];
        cell.delegate = self;
    }else{
        //Fetch Data
        NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"SaveID"
                                                  inManagedObjectContext:self.context];
        [fetchRequest setEntity:entity];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"newsID = %@", [[self.arrAllNews valueForKey:@"id"] objectAtIndex:index]]];
        NSArray *results = [self.context executeFetchRequest:fetchRequest error:nil];
        for (NSManagedObject *object in results) {
            [self.context deleteObject:object];
        }
        //Notification to change the image in panning
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:[NSNumber numberWithBool:NO] forKey:@"isFavorited"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"isFavorited" object:dict];
    }
    
    NSError *err, *error;
    if (![self.context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [err localizedDescription]);
    }
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)
        [self.cvDashboard reloadData];
    else
        [self.tbDashboard reloadData];
}

- (void)share:(NSNotification*)notification
{
    
    DMActivityInstagram *instagramActivity = [[DMActivityInstagram alloc] init];
    TMTumblrActivity *tumblrActivity = [[TMTumblrActivity alloc] init];
    
    NSString *message = self.strTitle;
    NSArray *shareItems = @[message];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:shareItems applicationActivities:@[instagramActivity,tumblrActivity]];
    [self presentViewController:activityVC animated:YES completion:nil];
}

#pragma mark - Alert
-(void)dismiss:(UIAlertView*)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchController.searchResultsTableView)
        return self.arrSearchResults.count;
    else
        return self.arrNews.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.searchController.searchResultsTableView) {
        //Check if iPad
        NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
        if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)
            return 80;
        else
            return 62;
    } else if (tableView == self.tbDashboard) {
        return  200;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == self.searchController.searchResultsTableView) {
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        SearchCell *searchCell = [[[NSBundle mainBundle] loadNibNamed:@"SearchCell" owner:self options:nil] lastObject];
        [searchCell setSelectionStyle:UITableViewCellSelectionStyleGray];
        searchCell.lblTitle.text = [[self.arrSearchResults valueForKey:@"title"] objectAtIndex:indexPath.row];
        
        //Company
        NSString *strCompany = @"";
        if ([[self.arrSearchResults valueForKey:@"company"] objectAtIndex:indexPath.row] != [NSNull null])
            strCompany = [[self.arrSearchResults valueForKey:@"company"] objectAtIndex:indexPath.row];
        //URL
        //    NSRange nameRange = [[[self.arrSearch valueForKey:@"url"] objectAtIndex:indexPath.row] rangeOfString:@"com" options:NSCaseInsensitiveSearch];
        //    NSString *strURL;
        //    if(nameRange.location != NSNotFound)
        //    {
        //        strURL = [[[self.arrSearch valueForKey:@"url"] objectAtIndex:indexPath.row] substringWithRange:NSMakeRange(0, (nameRange.location+nameRange.length))];
        //        //[self.arrSearchResults addObject:item];
        //    }else{
        //        strURL = @"";
        //    }
        
        //Date
        if ([[self.arrSearchResults valueForKey:@"date"] objectAtIndex:indexPath.row] != [NSNull null]){
            NSString *dateInString = [[self.arrSearchResults valueForKey:@"date"] objectAtIndex:indexPath.row];
            NSString *yearString = [dateInString substringWithRange:NSMakeRange(0, 4)];
            NSString *monthString = [dateInString substringWithRange:NSMakeRange(5, 2)];
            NSString *dayString = [dateInString substringWithRange:NSMakeRange(8, 2)];
            monthString = [Utility getMonthShort:monthString];
            searchCell.lblURL.text = [NSString stringWithFormat:@"%@ - %@ %@, %@", strCompany,monthString,dayString,yearString];
            //searchCell.lblURL.text = [NSString stringWithFormat:@"%@ - %@", strCompany,[[self.arrSearch valueForKey:@"date"] objectAtIndex:indexPath.row]];
        } else
            searchCell.lblURL.text = [NSString stringWithFormat:@"%@", strCompany];
        
        if ([[self.arrSearchResults valueForKey:@"comment"] objectAtIndex:indexPath.row] != [NSNull null])
            searchCell.lblComment.text = [[self.arrSearchResults valueForKey:@"comment"] objectAtIndex:indexPath.row];
        else
            searchCell.lblComment.text = @"";
        
        return searchCell;
    }else{
        //CoreData
        self.appDelegate = [[UIApplication sharedApplication] delegate];
        self.context = [self.appDelegate managedObjectContext];
        
        //Check if Favorited
        NSError *error;
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"SaveID"
                                                  inManagedObjectContext:self.context];
        [fetchRequest setEntity:entity];
        NSArray *fetchedObjects = [self.context executeFetchRequest:fetchRequest error:&error];
        for (SaveID *saveID in fetchedObjects) {
            if ([saveID.newsID intValue] == [[[self.arrNews valueForKey:@"id"] objectAtIndex:indexPath.row] intValue]) {
                self.isFavorited = YES;
                break;
            }else{
                self.isFavorited = NO;
            }
        }
        if (fetchedObjects.count == 0) {
            self.isFavorited = NO;
        }
        
        //Check if iPad
        NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
        if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound){
            static NSString *CellIdentifier = @"CellIdentifier";
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            return cell;
        }else{
            //Categories
            NSArray *categories = [[[self.arrNews valueForKey:@"category"] objectAtIndex:indexPath.row] componentsSeparatedByString:@","];
            NSString *path = [[NSBundle mainBundle] pathForResource:@"Categories" ofType:@"plist"];
            NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
            NSArray *arrName = [dict valueForKey:@"Name"];
            
            self.strCategories = [NSMutableString string];
            for (NSString *num in categories) {
                NSString *pureNumbers = [[num componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
                
                [self.strCategories appendString:[NSString stringWithFormat:@"%@  ", [arrName objectAtIndex:[pureNumbers intValue]]]];
            }
            
            HHPanningTableViewCell *dashboardCell = [[[NSBundle mainBundle] loadNibNamed:@"DashboardCell" owner:self options:nil] lastObject];
            UIView *drawerView = [[UIView alloc] initWithFrame:dashboardCell.frame];
            dashboardCell.drawerView = drawerView;
            dashboardCell.directionMask = HHPanningTableViewCellDirectionLeft;
            dashboardCell.isFavorited = self.isFavorited;
            [dashboardCell setCellDataWithIndex:indexPath.row
                                          title:[[self.arrNews valueForKey:@"title"] objectAtIndex:indexPath.row]
                                        comment:[[self.arrNews valueForKey:@"comment"] objectAtIndex:indexPath.row]
                                        company:[[self.arrNews valueForKey:@"company"] objectAtIndex:indexPath.row]
                                       category:self.strCategories
                                          image:[[self.arrNews valueForKey:@"mainImage"] objectAtIndex:indexPath.row]
                                           vote:[[self.arrNews valueForKey:@"votes"] objectAtIndex:indexPath.row]];
            self.strTitle = [[self.arrNews valueForKey:@"title"] objectAtIndex:indexPath.row];
            self.strURL = [[self.arrNews valueForKey:@"url"] objectAtIndex:indexPath.row];
            
            NSURL *url = [NSURL URLWithString:[[self.arrNews valueForKey:@"mainImage"] objectAtIndex:indexPath.row]];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            //UIImage *placeholderImage = [UIImage imageNamed:@""];

            [dashboardCell.imageView setImageWithURLRequest:request
                                  placeholderImage:nil
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                               
                                               //weakCell.imageView.image = image;
                                               dashboardCell.imgViewImage.image = image;
                                               [dashboardCell setNeedsLayout];
                                               
                                           } failure:nil];
            
            return dashboardCell;
        }
    }

    

	return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.searchDisplayController.searchBar resignFirstResponder];
    
    //Push to News Details
    NewsDetailViewController *newsDetail = [[NewsDetailViewController alloc] initWithNibName:@"NewsDetailViewController" bundle:nil];
    newsDetail.arrNewsContent = [self.arrNews objectAtIndex:indexPath.row];
    newsDetail.arrAllNews = self.arrNews;
    newsDetail.index = indexPath.row;
    
    [self.navigationController pushViewController:newsDetail animated:YES];
}

#pragma mark - Collection view data source
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.arrAllNews count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    //CoreData
    self.appDelegate = [[UIApplication sharedApplication] delegate];
    self.context = [self.appDelegate managedObjectContext];
    
    //Check if Favorited
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SaveID"
                                              inManagedObjectContext:self.context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [self.context executeFetchRequest:fetchRequest error:&error];
    for (SaveID *saveID in fetchedObjects) {
        if ([saveID.newsID intValue] == [[[self.arrAllNews valueForKey:@"id"] objectAtIndex:indexPath.row] intValue]) {
            self.isFavorited = YES;
            break;
        }else{
            self.isFavorited = NO;
        }
    }
    if (fetchedObjects.count == 0) {
        self.isFavorited = NO;
    }
    
    // Custom Cell
    DashboardIpadCell *dashboardCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DashboardCell" forIndexPath:indexPath];
    [dashboardCell.lblTitle setText:[[self.arrAllNews valueForKey:@"title"] objectAtIndex:indexPath.row]];
    [dashboardCell.lblComment setText:[[self.arrAllNews valueForKey:@"comment"] objectAtIndex:indexPath.row]];
    
    //Categories
    NSArray *categories = [[[self.arrAllNews valueForKey:@"category"] objectAtIndex:indexPath.row] componentsSeparatedByString:@","];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Categories" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    NSArray *arrName = [dict valueForKey:@"Name"];
    
    self.strCategories = [NSMutableString string];
    for (NSString *num in categories) {
        NSString *pureNumbers = [[num componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        
        [self.strCategories appendString:[NSString stringWithFormat:@"%@  ", [arrName objectAtIndex:[pureNumbers intValue]]]];
    }
    
    NSString *strCompanyCategory = [NSString stringWithFormat:@"%@     %@",[[self.arrAllNews valueForKey:@"company"] objectAtIndex:indexPath.row], self.strCategories];
    [dashboardCell.lblCompany setText:strCompanyCategory];
    
    if ([[self.arrAllNews valueForKey:@"mainImage"] objectAtIndex:indexPath.row] != [NSNull null]) {
        NSURL *url = [NSURL URLWithString:[[self.arrAllNews valueForKey:@"mainImage"] objectAtIndex:indexPath.row]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [dashboardCell.imgViewImage setImageWithURLRequest:request
                                   placeholderImage:nil
                                            success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                
                                                //weakCell.imageView.image = image;
                                                dashboardCell.imgViewImage.image = image;
                                                [dashboardCell setNeedsLayout];
                                                
                                            } failure:nil];
    }
    [dashboardCell setImage:self.isFavorited];
    
	return dashboardCell;

}

/*- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    SearchCell *searchCell = [collectionView dequeueReusableSupplementaryViewOfKind:
                              UICollectionElementKindSectionHeader withReuseIdentifier:@"SearchCell" forIndexPath:indexPath];

    searchCell.lblTitle.text = [[self.arrSearchResults valueForKey:@"title"] objectAtIndex:indexPath.row];
    
    //Company
    NSString *strCompany = @"";
    if ([[self.arrSearchResults valueForKey:@"company"] objectAtIndex:indexPath.row] != [NSNull null])
        strCompany = [[self.arrSearchResults valueForKey:@"company"] objectAtIndex:indexPath.row];
    //URL
    //    NSRange nameRange = [[[self.arrSearch valueForKey:@"url"] objectAtIndex:indexPath.row] rangeOfString:@"com" options:NSCaseInsensitiveSearch];
    //    NSString *strURL;
    //    if(nameRange.location != NSNotFound)
    //    {
    //        strURL = [[[self.arrSearch valueForKey:@"url"] objectAtIndex:indexPath.row] substringWithRange:NSMakeRange(0, (nameRange.location+nameRange.length))];
    //        //[self.arrSearchResults addObject:item];
    //    }else{
    //        strURL = @"";
    //    }
    
    //Date
    if ([[self.arrSearchResults valueForKey:@"date"] objectAtIndex:indexPath.row] != [NSNull null]){
        NSString *dateInString = [[self.arrSearchResults valueForKey:@"date"] objectAtIndex:indexPath.row];
        NSString *yearString = [dateInString substringWithRange:NSMakeRange(0, 4)];
        NSString *monthString = [dateInString substringWithRange:NSMakeRange(5, 2)];
        NSString *dayString = [dateInString substringWithRange:NSMakeRange(8, 2)];
        monthString = [Utility getMonthShort:monthString];
        searchCell.lblURL.text = [NSString stringWithFormat:@"%@ - %@ %@, %@", strCompany,monthString,dayString,yearString];
        //searchCell.lblURL.text = [NSString stringWithFormat:@"%@ - %@", strCompany,[[self.arrSearch valueForKey:@"date"] objectAtIndex:indexPath.row]];
    } else
        searchCell.lblURL.text = [NSString stringWithFormat:@"%@", strCompany];
    
    if ([[self.arrSearchResults valueForKey:@"comment"] objectAtIndex:indexPath.row] != [NSNull null])
        searchCell.lblComment.text = [[self.arrSearchResults valueForKey:@"comment"] objectAtIndex:indexPath.row];
    else
        searchCell.lblComment.text = @"";
    
    return searchCell;
}*/

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    //Push to News Details
    NewsDetailViewController *newsDetail = [[NewsDetailViewController alloc] initWithNibName:@"NewsDetailViewController" bundle:nil];
    newsDetail.arrNewsContent = [self.arrAllNews objectAtIndex:indexPath.row];
    newsDetail.arrAllNews = self.arrAllNews;
    newsDetail.index = indexPath.row;
    
    [self.navigationController pushViewController:newsDetail animated:YES];
}

#pragma mark - Notification
- (void)getAllNewsSuccess:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"allNewsSuccess" object:nil];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"News"
                                              inManagedObjectContext:self.context];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSArray *fetchedObjects = [self.context executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *allNews = [[NSMutableArray alloc]init];
    self.arrAllNews = [[NSMutableArray alloc]init];
    for (News *news in fetchedObjects) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:news.newsID forKey:@"id"];
        [dict setValue:news.title forKey:@"title"];
        [dict setValue:news.url forKey:@"url"];
        [dict setValue:news.company forKey:@"company"];
        [dict setValue:news.comment forKey:@"comment"];
        [dict setValue:news.mainImage forKey:@"mainImage"];
        [dict setValue:news.category forKey:@"category"];
        [dict setValue:news.content forKey:@"content"];
        [dict setValue:news.postedDate forKey:@"created_at"];
        [dict setValue:news.vote forKey:@"votes"];
        [allNews addObject:dict];
    }
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"id" ascending:YES];
    [allNews sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    
    //Filter News by Category
    if (selectedIndex == 0) {
         for (id item in allNews) {
             [self.arrAllNews addObject:item];
         }
    }else{
        for (id item in allNews) {
            NSArray *categories = [[item valueForKey:@"category"] componentsSeparatedByString:@","];

            for (NSString *num in categories) {
                NSString *pureNumbers = [[num componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
                
                if (selectedIndex == [pureNumbers intValue]) {
                    [self.arrAllNews addObject:item];
                    break;
                }
            }
        }
    }
    
    //Check if No Available News
    if (self.arrAllNews.count == 0) {
        [self.noNews setHidden:NO];
    }else{
        [self.noNews setHidden:YES];
    }
    
    //Copy contents of arrAllNews
    self.arrNews = [[NSMutableArray alloc]init];
    self.arrNews = [self.arrAllNews copy];
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)
        [self.cvDashboard reloadData];
    else
        [self.tbDashboard reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
