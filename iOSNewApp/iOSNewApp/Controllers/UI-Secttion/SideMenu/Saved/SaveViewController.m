//
//  SaveViewController.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/11/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "SaveViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "SaveID.h"
#import "News.h"
#import "SavedCell.h"
#import "NewsDetailViewController.h"
#import "Utility.h"
#import "UIImageView+AFNetworking.h"

@interface SaveViewController ()

@property (nonatomic,strong) IBOutlet UITableView *tbSaved;
@property (nonatomic,strong) NSMutableArray *arrSaved;
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic, strong) NSManagedObjectContext *context;

@end

AppDelegate *appDelegate;
@implementation SaveViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-menu.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:146/255.0 green:201/255.0 blue:197/255.0 alpha:1.0];
    self.navigationItem.title = @"Saved";
    
    //TableView
    self.tbSaved.dataSource = self;
    self.tbSaved.delegate = self;
    
    
    //CoreData
    self.appDelegate = [[UIApplication sharedApplication] delegate];
    self.context = [self.appDelegate managedObjectContext];
    
    //Fetch newsID in coredata SaveID
    NSFetchRequest *saveRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *saveEntity = [NSEntityDescription entityForName:@"SaveID"
                                              inManagedObjectContext:self.context];
    [saveRequest setEntity:saveEntity];
    NSError *error;
    NSArray *saveObjects = [self.context executeFetchRequest:saveRequest error:&error];
    
    NSFetchRequest *newsRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *newsEntity = [NSEntityDescription entityForName:@"News"
                                                  inManagedObjectContext:self.context];
    [newsRequest setEntity:newsEntity];
    NSArray *newsObjects = [self.context executeFetchRequest:newsRequest error:&error];
    
    self.arrSaved = [[NSMutableArray alloc]init];
    for (SaveID *save in saveObjects) {
        for (News *news in newsObjects) {
            if (news.newsID == save.newsID) {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
                [dict setValue:news.newsID forKey:@"id"];
                [dict setValue:news.title forKey:@"title"];
                [dict setValue:news.url forKey:@"url"];
                [dict setValue:news.company forKey:@"company"];
                [dict setValue:news.comment forKey:@"comment"];
                [dict setValue:news.mainImage forKey:@"mainImage"];
                [dict setValue:news.category forKey:@"category"];
                [dict setValue:news.content forKey:@"content"];
                [dict setValue:save.saveDate forKey:@"saveDate"];
                [dict setValue:news.vote forKey:@"votes"];
                [self.arrSaved addObject:dict];
            }
        }
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"saveDate" ascending:NO];
    [self.arrSaved sortUsingDescriptors:[NSArray arrayWithObject:sort]];
}

#pragma mark - Device Orientation
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.tbSaved reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrSaved.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SavedCell *savedCell = [[[NSBundle mainBundle] loadNibNamed:@"SavedCell" owner:self options:nil] lastObject];
    [savedCell setSelectionStyle:UITableViewCellSelectionStyleGray];
    savedCell.lblTitle.text = [[self.arrSaved valueForKey:@"title"] objectAtIndex:indexPath.row];
    
    if ([[self.arrSaved valueForKey:@"mainImage"] objectAtIndex:indexPath.row] != [NSNull null]) {
        NSURL *url = [NSURL URLWithString:[[self.arrSaved valueForKey:@"mainImage"] objectAtIndex:indexPath.row]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [savedCell.imageView setImageWithURLRequest:request
                                       placeholderImage:nil
                                                success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                    
                                                    savedCell.imgViewSaved.image = image;
                                                    [savedCell setNeedsLayout];
                                                    
                                                } failure:nil];
        
    }
    
    NSString *strDateDiff = [Utility getPostDateDifference:[[self.arrSaved valueForKey:@"saveDate"] objectAtIndex:indexPath.row]];
    savedCell.lblCompany.text = [NSString stringWithFormat:@"%@ - %@",[[self.arrSaved valueForKey:@"company"] objectAtIndex:indexPath.row],strDateDiff];
    
    savedCell.lblComment.text = [[self.arrSaved valueForKey:@"comment"] objectAtIndex:indexPath.row];

    return savedCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NewsDetailViewController *newsDetail = [[NewsDetailViewController alloc] initWithNibName:@"NewsDetailViewController" bundle:nil];
    newsDetail.arrNewsContent = [self.arrSaved objectAtIndex:indexPath.row];
    newsDetail.arrAllNews = self.arrSaved;
    newsDetail.index = indexPath.row;
    [self.navigationController pushViewController:newsDetail animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
