//
//  InviteFriendsViewController.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/11/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "InviteFriendsViewController.h"
#import "SWRevealViewController.h"

@interface InviteFriendsViewController ()

@end

@implementation InviteFriendsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-menu.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:146/255.0 green:201/255.0 blue:197/255.0 alpha:1.0];
    self.navigationItem.title = @"Invite Friends";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
