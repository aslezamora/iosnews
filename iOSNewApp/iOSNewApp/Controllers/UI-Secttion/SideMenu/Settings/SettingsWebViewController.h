//
//  SettingsWebViewController.h
//  iOSNewApp
//
//  Created by Zamora, Lea Asle K. on 4/30/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsWebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) NSString *strTitle;

@end
