//
//  SettingsViewController.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/11/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "SettingsViewController.h"
#import "SWRevealViewController.h"
#import "SettingsCell.h"
#import "SettingsWebViewController.h"

@interface SettingsViewController ()

@property (nonatomic, strong) IBOutlet UITableView *tbSettings;
@property (nonatomic, strong) NSArray *arrSettings;//, *arrSettingsImage;

@end

@implementation SettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)  {
        UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"DONE"
                                                                            style:UIBarButtonItemStyleBordered
                                                                           target:self
                                                                           action:@selector(done:)];
        
        self.navigationItem.rightBarButtonItem = doneButtonItem;
        self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithRed:146/255.0 green:201/255.0 blue:197/255.0 alpha:1.0];
        self.navigationItem.title = @"Settings";
    }else{
        SWRevealViewController *revealController = [self revealViewController];
        
        [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
        
        UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-menu.png"]
                                                                             style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
        
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:146/255.0 green:201/255.0 blue:197/255.0 alpha:1.0];
        self.navigationItem.title = @"Settings";
    }
    
    //Table
    self.tbSettings.dataSource = self;
    self.tbSettings.delegate = self;
    self.tbSettings.bounces = NO;
    self.arrSettings = [[NSArray alloc]initWithObjects:@"Notifications",@"Send Feedback",@"Rate Us",@"About",@"Term of Service",@"Privacy Policy", nil];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)  {
        self.view.superview.bounds = CGRectMake(0, 0, 500, 600);
    }
}

#pragma mark - Buttons

- (IBAction)done:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrSettings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsCell *settingsCell = [[[NSBundle mainBundle] loadNibNamed:@"SettingsCell" owner:self options:nil] lastObject];
    [settingsCell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [settingsCell setCellDataWithIndex:indexPath.row name:self.arrSettings];
	
	return settingsCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *strSettings;
    if ([[self.arrSettings objectAtIndex:indexPath.row] isEqualToString:@"About"])
        strSettings = [self.arrSettings objectAtIndex:indexPath.row];
    else if ([[self.arrSettings objectAtIndex:indexPath.row] isEqualToString:@"Term of Service"])
        strSettings = [self.arrSettings objectAtIndex:indexPath.row];
    else if ([[self.arrSettings objectAtIndex:indexPath.row] isEqualToString:@"Privacy Policy"])
        strSettings = [self.arrSettings objectAtIndex:indexPath.row];
    
    //Push
    SettingsWebViewController *settingsWeb = [[SettingsWebViewController alloc] initWithNibName:@"SettingsWebViewController" bundle:nil];
    settingsWeb.strTitle = strSettings;
    [self.navigationController pushViewController:settingsWeb animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
