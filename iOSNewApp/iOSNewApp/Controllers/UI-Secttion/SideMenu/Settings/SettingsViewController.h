//
//  SettingsViewController.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/11/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
