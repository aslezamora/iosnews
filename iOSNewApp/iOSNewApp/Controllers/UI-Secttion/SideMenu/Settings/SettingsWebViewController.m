//
//  SettingsWebViewController.m
//  iOSNewApp
//
//  Created by Zamora, Lea Asle K. on 4/30/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "SettingsWebViewController.h"
#import "SWRevealViewController.h"
#import "MBProgressHUD.h"

@interface SettingsWebViewController ()

@property (nonatomic, strong) IBOutlet UIWebView *webSettings;

@end

@implementation SettingsWebViewController
@synthesize strTitle;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-back.png"]
                                                                       style:UIBarButtonItemStyleBordered target:self action:@selector(backToMain:)];
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:146/255.0 green:201/255.0 blue:197/255.0 alpha:1.0];
    self.navigationItem.title = strTitle;
    
    self.webSettings.delegate = self;
    
    //Get link from Environmental.pList
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Environmental" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];

    NSString *strSettings = @"";
    for (id settings in dict) {
        if ([settings isEqualToString:@"About"]){
            strSettings = [dict objectForKey:settings];
            break;
        } else if ([settings isEqualToString:@"Terms"]){
            strSettings = [dict objectForKey:settings];
            break;
        }else{
            strSettings = [dict objectForKey:settings];
            break;
        }
    }
    
    NSURL *url = [NSURL URLWithString:strSettings];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webSettings loadRequest:requestObj];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)  {
        self.view.superview.bounds = CGRectMake(0, 0, 500, 600);
    }
}

#pragma mark - Buttons
- (IBAction)backToMain:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - WebView
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webview{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
