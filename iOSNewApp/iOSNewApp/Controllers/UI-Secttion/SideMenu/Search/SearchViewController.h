//
//  SearchViewController.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 3/16/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSString *strSearchKeyword;
@property (nonatomic, strong) NSMutableArray *arrSearch;

@end
