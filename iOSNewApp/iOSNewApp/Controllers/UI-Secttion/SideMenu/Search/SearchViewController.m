//
//  SearchViewController.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 3/16/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "SearchViewController.h"
#import "NewsDetailViewController.h"
#import "SearchCell.h"
#import "Utility.h"
#import "DashboardData.h"
#import "MBProgressHUD.h"
#import "UIImageView+AFNetworking.h"

@interface SearchViewController ()

@property (nonatomic, strong) IBOutlet UITableView *tbSearch;
@property (nonatomic, strong) MBProgressHUD* hud;

@end

@implementation SearchViewController

@synthesize arrSearch, strSearchKeyword;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tbSearch.delegate = self;
    self.tbSearch.dataSource = self;
    
   [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //search result
    [[DashboardData sharedInstance] getsSearchResults:self.strSearchKeyword];
    
    //Notification Observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getSearchResultSuccess:)
                                                 name:@"searchResultSuccess"
                                               object:nil];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrSearch.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)
        return 100;
    else
        return 60;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SearchCell *searchCell = [[[NSBundle mainBundle] loadNibNamed:@"SearchCell" owner:self options:nil] lastObject];
    [searchCell setSelectionStyle:UITableViewCellSelectionStyleGray];
    searchCell.lblTitle.text = [[self.arrSearch valueForKey:@"title"] objectAtIndex:indexPath.row];
    //Date
    //NSString *strDateDiff = [Utility getPostDateDifference:[[self.arrSearch valueForKey:@"created_at"] objectAtIndex:indexPath.row]];
    
    //Image
    if ([[self.arrSearch valueForKey:@"mainImage"] objectAtIndex:indexPath.row] != [NSNull null]) {
        NSURL *url = [NSURL URLWithString:[[self.arrSearch valueForKey:@"mainImage"] objectAtIndex:indexPath.row]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [searchCell.imageView setImageWithURLRequest:request
                                   placeholderImage:nil
                                            success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                
                                                //weakCell.imageView.image = image;
                                                searchCell.imgViewSearch.image = image;
                                                [searchCell setNeedsLayout];
                                                
                                            } failure:nil];
        
    }

    
    //Company
    NSString *strCompany = @"";
    if ([[self.arrSearch valueForKey:@"company"] objectAtIndex:indexPath.row] != [NSNull null])
        strCompany = [[self.arrSearch valueForKey:@"company"] objectAtIndex:indexPath.row];
    //URL
//    NSRange nameRange = [[[self.arrSearch valueForKey:@"url"] objectAtIndex:indexPath.row] rangeOfString:@"com" options:NSCaseInsensitiveSearch];
//    NSString *strURL;
//    if(nameRange.location != NSNotFound)
//    {
//        strURL = [[[self.arrSearch valueForKey:@"url"] objectAtIndex:indexPath.row] substringWithRange:NSMakeRange(0, (nameRange.location+nameRange.length))];
//        //[self.arrSearchResults addObject:item];
//    }else{
//        strURL = @"";
//    }
    
    if ([[self.arrSearch valueForKey:@"date"] objectAtIndex:indexPath.row] != [NSNull null]){
        NSString *dateInString = [[self.arrSearch valueForKey:@"date"] objectAtIndex:indexPath.row];
        NSString *yearString = [dateInString substringWithRange:NSMakeRange(0, 4)];
        NSString *monthString = [dateInString substringWithRange:NSMakeRange(5, 2)];
        NSString *dayString = [dateInString substringWithRange:NSMakeRange(8, 2)];
        monthString = [Utility getMonthShort:monthString];
        searchCell.lblURL.text = [NSString stringWithFormat:@"%@ - %@ %@, %@", strCompany,monthString,dayString,yearString];
        //searchCell.lblURL.text = [NSString stringWithFormat:@"%@ - %@", strCompany,[[self.arrSearch valueForKey:@"date"] objectAtIndex:indexPath.row]];
    } else
        searchCell.lblURL.text = [NSString stringWithFormat:@"%@", strCompany];
    
    if ([[self.arrSearch valueForKey:@"comment"] objectAtIndex:indexPath.row] != [NSNull null])
        searchCell.lblComment.text = [[self.arrSearch valueForKey:@"comment"] objectAtIndex:indexPath.row];
    else
        searchCell.lblComment.text = @"";
    
    return searchCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //Push to News Details
    NewsDetailViewController *newsDetail = [[NewsDetailViewController alloc] initWithNibName:@"NewsDetailViewController" bundle:nil];
    newsDetail.arrNewsContent = [self.arrSearch objectAtIndex:indexPath.row];
    newsDetail.arrAllNews = self.arrSearch;
    newsDetail.index = indexPath.row;
    
    [self.navigationController pushViewController:newsDetail animated:YES];
}

#pragma mark - Notification
- (void)getSearchResultSuccess:(NSNotification*)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"searchResultSuccess" object:nil];
    self.arrSearch = [[DashboardData sharedInstance] getSearchResultArray];
    //[self.tbSearch reloadData];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
