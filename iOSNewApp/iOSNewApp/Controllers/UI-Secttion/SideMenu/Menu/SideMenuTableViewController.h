//
//  SideMenuTableViewController.h
//  iOSApp
//
//  Created by cpccqo143461 on 2/10/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@end
