//
//  SideMenuTableViewController.m
//  iOSApp
//
//  Created by cpccqo143461 on 2/10/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "SideMenuTableViewController.h"
#import "SWRevealViewController.h"
#import "Dashboard.h"
#import "SideMenuCell.h"
#import "SaveViewController.h"
#import "InviteFriendsViewController.h"
#import "SettingsViewController.h"
#import "Constant.h"

@interface SideMenuTableViewController (){
    NSInteger _presentedRow;
}

@property (nonatomic, strong) IBOutlet UITableView *tbSideMenu;
@property (nonatomic, strong) IBOutlet UILabel *lblInvite, *lblSettings;
@property (nonatomic, strong) NSArray *arrSideMenuName, *arrSideMenuImg;
@property (nonatomic,strong) UIPopoverController *popover;

@end

@implementation SideMenuTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // We determine whether we have a grand parent SWRevealViewController, this means we are at least one level behind the hierarchy
    SWRevealViewController *parentRevealController = self.revealViewController;
    SWRevealViewController *grandParentRevealController = parentRevealController.revealViewController;
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button-menu.png"]
                                                                         style:UIBarButtonItemStyleBordered target:grandParentRevealController action:@selector(revealToggle:)];
    
    // if we have a reveal controller as a grand parent, this means we are are being added as a
    // child of a detail (child) reveal controller, so we add a gesture recognizer provided by our grand parent to our
    // navigation bar as well as a "reveal" button, we also set
    if ( grandParentRevealController )
    {
        // to present a title, we count the number of ancestor reveal controllers we have, this is of course
        // only a hack for demonstration purposes, on a real project you would have a model telling this.
        int level=0;
        UIViewController *controller = grandParentRevealController;
        while( nil != (controller = [controller revealViewController]) )
            level++;
        
        NSString *title = [NSString stringWithFormat:@"Detail Level %d", level];
        
        [self.navigationController.navigationBar addGestureRecognizer:grandParentRevealController.panGestureRecognizer];
        self.navigationItem.leftBarButtonItem = revealButtonItem;
        self.navigationItem.title = title;
    }

    //SideMenu Array
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Categories" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    self.arrSideMenuName = [dict valueForKey:@"Name"];
    self.arrSideMenuImg = [dict valueForKey:@"Image"];
    
    //self.arrSideMenuName = [[NSArray alloc]initWithObjects:@"Top Stories", @"Video", @"Saved", @"", @"Invite Friends", @"Settings", nil];
    //self.arrSideMenuImg = [[NSArray alloc]initWithObjects:@"button-mainfeed.png", @"button-video.png", @"button-favorite.png",@"", @"button-invite.png Friends", @"button-settings.png", nil];
    self.tbSideMenu.delegate = self;
    self.tbSideMenu.dataSource = self;
    self.tbSideMenu.bounces = NO;
    
    self.lblInvite.font = [UIFont fontWithName:font_sideMenu size:18.0];
    self.lblSettings.font = [UIFont fontWithName:font_sideMenu size:18.0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    SWRevealViewController *grandParentRevealController = self.revealViewController.revealViewController;
    grandParentRevealController.bounceBackOnOverdraw = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    SWRevealViewController *grandParentRevealController = self.revealViewController.revealViewController;
    grandParentRevealController.bounceBackOnOverdraw = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Buttons
- (IBAction)inviteFriends:(id)sender{
    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;
    
    InviteFriendsViewController *inviteFriendsViewController = [[InviteFriendsViewController alloc] init];
    newFrontController = [[UINavigationController alloc] initWithRootViewController:inviteFriendsViewController];
    [revealController pushFrontViewController:newFrontController animated:YES];
}

- (IBAction)settings:(id)sender{
    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;
    
    //Check if iPad
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)  {
    
        SettingsViewController *settingsViewController = [[SettingsViewController alloc] init];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
        newFrontController.modalPresentationStyle = UIModalPresentationFormSheet;
        newFrontController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        newFrontController.preferredContentSize = CGSizeMake(500.0f, 600.0f);
        [revealController presentViewController:newFrontController animated:YES completion:nil];
    }else{
        SettingsViewController *settingsViewController = [[SettingsViewController alloc] init];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
        [revealController pushFrontViewController:newFrontController animated:YES];
    }
}

#pragma mark - Device Orientation
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.tbSideMenu reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrSideMenuName.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
    if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)  {
        return 60;
//        if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft || [[UIDevice currentDevice] orientation ]== UIDeviceOrientationLandscapeRight)
//        {
//            if (indexPath.row == 3)
//                return 404;
//            else
//                return 60;
//        }
//        if([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait || [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown )
//        {
//            if (indexPath.row == 3)
//                return 659;
//            else
//                return 60;
//        }
    }else{
        return 40;
//        if (indexPath.row == 3)
//            return 304;
//        else
//            return 40;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SideMenuCell *sideMenuCell = [[[NSBundle mainBundle] loadNibNamed:@"SideMenuCell" owner:self options:nil] lastObject];
    [sideMenuCell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [sideMenuCell setCellDataWithIndex:indexPath.row name:self.arrSideMenuName imageName:self.arrSideMenuImg];
//    if (indexPath.row == 3)
//        [sideMenuCell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    if (indexPath.row == 4){
////        UIView *vwTopSeparatorLine = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,1)];
////        vwTopSeparatorLine.backgroundColor = [UIColor colorWithRed:222/255.0 green:222/255.0 blue:222/255.0 alpha:1.0];
////        [sideMenuCell.contentView addSubview:vwTopSeparatorLine];
//    }
    
    //Set Selected Row by Default
    NSIndexPath *indexPathRow = [NSIndexPath indexPathForRow:0 inSection:0];
    [tableView selectRowAtIndexPath:indexPathRow animated:NO scrollPosition: UITableViewScrollPositionNone];
    
    return sideMenuCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SWRevealViewController *revealController = self.revealViewController;
    NSInteger row = indexPath.row;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    UIViewController *newFrontController = nil;
    
    if (row == 1){
        SaveViewController *savedViewController = [[SaveViewController alloc] init];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:savedViewController];
        [revealController pushFrontViewController:newFrontController animated:YES];
    }else{
        Dashboard *dashboardViewController = [[Dashboard alloc] init];
        newFrontController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
        dashboardViewController.selectedIndex = indexPath.row;
        dashboardViewController.selectedCategory = [self.arrSideMenuName objectAtIndex:indexPath.row];
        [revealController pushFrontViewController:newFrontController animated:YES];
    }
    
    
//    if (row == 0){
//        Dashboard *dashboardViewController = [[Dashboard alloc] init];
//        newFrontController = [[UINavigationController alloc] initWithRootViewController:dashboardViewController];
//        [revealController pushFrontViewController:newFrontController animated:YES];
//    } else if (row == 1){
//        VideoViewController *videoViewController = [[VideoViewController alloc] init];
//        newFrontController = [[UINavigationController alloc] initWithRootViewController:videoViewController];
//        [revealController pushFrontViewController:newFrontController animated:YES];
//    } else if (row == 2){
//        SaveViewController *savedViewController = [[SaveViewController alloc] init];
//        newFrontController = [[UINavigationController alloc] initWithRootViewController:savedViewController];
//        [revealController pushFrontViewController:newFrontController animated:YES];
//    }else if (row == 3){
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//        [revealController setFrontViewPosition:FrontViewPositionRight animated:YES];
//        return;
//    } else if (row == 4){
//        InviteFriendsViewController *inviteFriendsViewController = [[InviteFriendsViewController alloc] init];
//        newFrontController = [[UINavigationController alloc] initWithRootViewController:inviteFriendsViewController];
//        [revealController pushFrontViewController:newFrontController animated:YES];
//    } else if (row == 5){
//        //Check if iPad
//        NSString *deviceModel = (NSString*)[UIDevice currentDevice].model;
//        if ([deviceModel rangeOfString:@"iPad"].location != NSNotFound)  {
//            SettingsViewController *settingsPopoverView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
//            
//            self.popover = [[UIPopoverController alloc] initWithContentViewController:settingsPopoverView];
//            self.popover.popoverContentSize = CGSizeMake(400.0f, 600.0f);
//            [self.popover presentPopoverFromRect:CGRectMake(-140.0f, 100.0f, self.popover.popoverContentSize.width, self.popover.popoverContentSize.height)
//                                               inView:revealController.view
//                             permittedArrowDirections:UIPopoverArrowDirectionLeft
//                                             animated:YES];
//            
//        }else{
//            SettingsViewController *settingsViewController = [[SettingsViewController alloc] init];
//            newFrontController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
//            [revealController pushFrontViewController:newFrontController animated:YES];
//        }
//    }

    //[revealController pushFrontViewController:newFrontController animated:YES];
    
    _presentedRow = row;
}


@end
