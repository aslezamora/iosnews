//
//  SearchCell.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 3/16/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitle, *lblURL, *lblComment;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewSearch;

@end
