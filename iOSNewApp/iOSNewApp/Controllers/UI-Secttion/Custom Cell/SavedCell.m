//
//  SavedCell.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/19/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "SavedCell.h"
#import "Utility.h"

@interface SavedCell ()

@end

@implementation SavedCell
@synthesize imgViewSaved, lblTitle, lblComment, lblCompany;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
