//
//  SavedCell.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/19/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblTitle, *lblCompany, *lblComment;
@property (nonatomic,strong) IBOutlet UIImageView *imgViewSaved;

@end
