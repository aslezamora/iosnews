//
//  SettingsCell.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/12/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell

-(void)setCellDataWithIndex:(NSInteger)index name:(NSArray *)arrSettingName;

@end
