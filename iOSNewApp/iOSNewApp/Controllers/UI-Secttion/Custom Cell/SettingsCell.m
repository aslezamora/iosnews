//
//  SettingsCell.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/12/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "SettingsCell.h"

@interface SettingsCell ()

@property (nonatomic,strong) IBOutlet UILabel *lblSetings;
@property (nonatomic,strong) IBOutlet UISwitch *switchNotification;

@end

@implementation SettingsCell

- (void)awakeFromNib
{
    // Initialization code
}

-(void)setCellDataWithIndex:(NSInteger)index name:(NSArray *)arrSettingName{
    self.lblSetings.text = [arrSettingName objectAtIndex:index];
    if (index == 0)
        self.switchNotification.hidden = NO;
    else
        self.switchNotification.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
