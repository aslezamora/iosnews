//
//  DashboardIpadCell.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 3/3/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardIpadCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UILabel *lblComment, *lblTitle, *lblCategory, *lblCompany, *lblVote;
@property (nonatomic,strong) IBOutlet UIImageView *imgViewImage, *imgViewFavorited;
//@property (nonatomic, assign) BOOL isFavorited;

-(void)setImage:(BOOL)isFavorited;


@end
