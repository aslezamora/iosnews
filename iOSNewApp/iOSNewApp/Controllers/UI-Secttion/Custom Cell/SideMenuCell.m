//
//  SideMenuCell.m
//  iOSApp
//
//  Created by cpccqo143461 on 2/11/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "SideMenuCell.h"
#import "Constant.h"

@interface SideMenuCell ()

@property (nonatomic,strong) IBOutlet UILabel *lblSideMenu;
@property (nonatomic,strong) IBOutlet UIImageView *imgViewSideMenu;
@property (nonatomic,strong) IBOutlet UIView *vwSeparatorLine;

@end

@implementation SideMenuCell
//@synthesize contentView;

- (void)awakeFromNib
{
    // Initialization code
}

-(void)setCellDataWithIndex:(NSInteger)index name:(NSArray *)arrSideMenuName imageName:(NSArray *)arrSideMenuImg{
    self.lblSideMenu.font = [UIFont fontWithName:font_sideMenu size:18.0];
    self.lblSideMenu.text = [arrSideMenuName objectAtIndex:index];
    self.imgViewSideMenu.image = [UIImage imageNamed:[arrSideMenuImg objectAtIndex:index]];
//    if (index == 3)
//        self.vwSeparatorLine.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
