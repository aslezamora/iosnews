//
//  DashboardIpadCell.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 3/3/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "DashboardIpadCell.h"

@implementation DashboardIpadCell
@synthesize lblCategory, lblComment, lblCompany, lblTitle, lblVote;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

    }
    return self;
}

-(void)setImage:(BOOL)isFavorited{
    if (isFavorited == YES){
        [self.imgViewFavorited setHidden:NO];
    }
    else{
        [self.imgViewFavorited setHidden:YES];
    }
    
}


@end
