//
//  SearchCell.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 3/16/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "SearchCell.h"

@implementation SearchCell
@synthesize imgViewSearch,lblTitle, lblURL, lblComment;

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
