//
//  DashboardData.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/13/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DashboardData : NSObject

@property (nonatomic, strong) NSArray *allNews;

+ (DashboardData *)sharedInstance;
- (void)getAllNews;
- (void)getsSearchResults:(NSString*)searchKeyword;
- (NSMutableArray *)getSearchResultArray;
- (NSMutableArray *):getSavedNews index:(NSInteger)index;


@end
