//
//  News.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/19/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "News.h"
#import "SaveID.h"


@implementation News

@dynamic author;
@dynamic category;
@dynamic comment;
@dynamic company;
@dynamic content;
@dynamic favorited;
@dynamic mainImage;
@dynamic newsID;
@dynamic postedDate;
@dynamic title;
@dynamic url;
@dynamic vote;

@end
