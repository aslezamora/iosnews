//
//  SaveID.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/19/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class News;

@interface SaveID : NSManagedObject

@property (nonatomic, retain) NSNumber * newsID;
@property (nonatomic, retain) NSDate *saveDate;

@end
