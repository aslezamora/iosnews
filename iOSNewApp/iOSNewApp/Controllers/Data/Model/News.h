//
//  News.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/19/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SaveID;

@interface News : NSManagedObject

@property (nonatomic, retain) NSString * author;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSNumber * favorited;
@property (nonatomic, retain) NSString * mainImage;
@property (nonatomic, retain) NSNumber * newsID;
@property (nonatomic, retain) NSString * postedDate;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * vote;

@end
