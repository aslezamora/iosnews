//
//  DashboardData.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/13/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "DashboardData.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "News.h"

@interface DashboardData()
@property (nonatomic,retain) NSMutableArray *arrSearch;
@property (nonatomic,strong) NSMutableArray *arrSavedNews;
@end

AppDelegate *appDelegate;
@implementation DashboardData

+ (DashboardData*)sharedInstance {
    static DashboardData* instance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        instance = [[DashboardData alloc] init];
    });
    return instance;
}

-(void)getAllNews{
    
    //NSURL *newsUrl = [NSURL URLWithString:@"http://manny-server.cloudapp.net/sites/mannysoft/barry/news/develop/public/api/v1/news"];
    NSURL *newsUrl = [NSURL URLWithString:@"http://contentsystem.gotdns.com/api/v1/news"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:newsUrl];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operation,
       id responseObject) {
         NSError *error;
         NSMutableDictionary *jsonDic = [NSJSONSerialization
                                         JSONObjectWithData:responseObject
                                         options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves
                                         error:&error];
         
         NSLog(@"%@",jsonDic);
         
         appDelegate = [[UIApplication sharedApplication] delegate];
         NSManagedObjectContext *context = [appDelegate managedObjectContext];
         
         NSManagedObject *newsDetails = nil;
         //for (NSManagedObject *news in [jsonDic objectForKey:@"data"]) {
         for (NSManagedObject *news in jsonDic) {
             NSFetchRequest * fetchRequest = [[NSFetchRequest alloc] init];
             NSEntityDescription *entity = [NSEntityDescription entityForName:@"News"
                                                       inManagedObjectContext:context];
             [fetchRequest setEntity:entity];
             [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"newsID = %@", [news valueForKey:@"id"]]];
             NSArray *results = [context executeFetchRequest:fetchRequest error:nil];
             if ([results count] == 0)
             {
                 newsDetails = [NSEntityDescription insertNewObjectForEntityForName:@"News" inManagedObjectContext:context];
                 [newsDetails setValue:[NSNumber numberWithInteger:[[news valueForKey:@"id"] integerValue]] forKey:@"newsID"];
                 [newsDetails setValue:[news valueForKey:@"title"] forKey:@"title"];
                 [newsDetails setValue:[news valueForKey:@"url"] forKey:@"url"];
                 [newsDetails setValue:[news valueForKey:@"company"] forKey:@"company"];
                 [newsDetails setValue:[news valueForKey:@"short_comment"] forKey:@"comment"];
                 //[newsDetails setValue:[NSData dataWithContentsOfURL:[NSURL URLWithString:[news valueForKey:@"main_image_url"]]] forKey:@"mainImage"];
                 [newsDetails setValue:[news valueForKey:@"main_image_url"] forKey:@"mainImage"];
                 [newsDetails setValue:[news valueForKey:@"categories"] forKey:@"category"];
                 [newsDetails setValue:[news valueForKey:@"content"] forKey:@"content"];
                 [newsDetails setValue:[news valueForKey:@"date"] forKey:@"postedDate"];
                 [newsDetails setValue:[news valueForKey:@"author"] forKey:@"author"];
                 [newsDetails setValue:[NSNumber numberWithInteger:[[news valueForKey:@"votes"] integerValue]] forKey:@"vote"];
             }
         }
         
         NSError *err;
         if (![context save:&error]) {
             NSLog(@"Whoops, couldn't save: %@", [err localizedDescription]);
         }

         
        [[NSNotificationCenter defaultCenter] postNotificationName:@"allNewsSuccess" object:self];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         NSLog(@"================= error =================");
         NSLog(@"JSON Response: %@", operation.error);
         NSLog(@"JSON Response: %@", operation.error.description);
         NSLog(@"JSON Response: %@", operation.error.localizedDescription);
         NSLog(@"================= error =================");
     }];
    
    [operation start];
}

-(void)getsSearchResults:(NSString*)searchKeyword{
    self.arrSearch = [[NSMutableArray alloc]init];
    NSURL *newsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://contentsystem.gotdns.com/api/v1/news?q=%@",searchKeyword]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:newsUrl];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:
     ^(AFHTTPRequestOperation *operation,
       id responseObject) {
         NSError *error;
         NSMutableDictionary *jsonDic = [NSJSONSerialization
                                         JSONObjectWithData:responseObject
                                         options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves
                                         error:&error];
         
         NSLog(@"%@",jsonDic);
         
         for (id item in jsonDic) {
             [self.arrSearch addObject:item];
         }
         
         [self getSearchResultArray];
         [[NSNotificationCenter defaultCenter] postNotificationName:@"searchResultSuccess" object:self];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         NSLog(@"================= error =================");
         NSLog(@"JSON Response: %@", operation.error);
         NSLog(@"JSON Response: %@", operation.error.description);
         NSLog(@"JSON Response: %@", operation.error.localizedDescription);
         NSLog(@"================= error =================");
     }];
    
    [operation start];
}

- (NSMutableArray *)getSearchResultArray{
    
    return self.arrSearch;
}

- (NSMutableArray *):getSavedNews index:(NSInteger)index;{
    self.arrSavedNews = [[NSMutableArray alloc]init];
    
    return self.arrSavedNews;
}

@end
