//
//  SFViewController.m
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 12/19/14.
//  Copyright (c) 2014 SurrealFacts. All rights reserved.
//

#import "SFViewController.h"
#import "AppInfoModel.h"
#import "RuleModel.h"
#import "GPAdController.h"
#import <TMCache/TMCache.h>
//#import <AdColony/AdColony.h>
#import <Chartboost/Chartboost.h>
#import <iRate/iRate.h>

typedef enum
{
    FullScreen_AdColony,
    FullScreen_ChartBoost,
    FullScreen_Both,
    FullScreen_None
} FullScreenAdType;

static NSString* const rateTitle = @"";
static NSString* const rateMessage = @"";
static NSString* const rateButtonTitle = @"";
static NSString* const rateButtonCancelTitle = @"";
static NSString* const rateButtonReminderTitle = @"";
static int const appstoreId = 123;


@interface SFViewController () <iRateDelegate>

@end

@implementation SFViewController
{
    FullScreenAdType fullScreenAdType;
}

+ (void)initialize
{
    [iRate sharedInstance].messageTitle = rateTitle;
    [iRate sharedInstance].message = rateMessage;
    [iRate sharedInstance].rateButtonLabel = rateButtonTitle;
    [iRate sharedInstance].cancelButtonLabel = rateButtonCancelTitle;
    [iRate sharedInstance].remindButtonLabel = rateButtonReminderTitle;
    [iRate sharedInstance].appStoreID = appstoreId;
    [iRate sharedInstance].daysUntilPrompt = 0.0f;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    fullScreenAdType = FullScreen_None;
    self.fullscreenAdTrigger = 0;
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [[GPAdController sharedController] getAppVersionsWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSData *responseData = (NSData *)responseObject;
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                             options:kNilOptions
                                                               error:&error];
        
        if(error == nil)
        {
            if([[json objectForKey:@"Status"] intValue] == 201)
            {
                NSArray *appVersions = [[NSArray alloc]initWithArray:[json objectForKey:@"Data"]];
                
                if(appVersions != nil)
                {
                    for(NSDictionary *dict in appVersions)
                    {
                        AppInfoModel *appInfo = [[AppInfoModel alloc] init];
                        [appInfo mts_setValuesForKeysWithDictionary:dict];
                        
                        //Get only the version number that the user is using
                        if([appInfo.versionNumber isEqualToString:version])
                        {
                            [self beginLoadingRulesForVerID:appInfo.ID];
                        }
                    }
                }
            }
        } 
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"ADS - getAppVersionsWithSuccess [Error: %@]", error.description);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[LARSAdController sharedManager] addAdContainerToViewInViewController:self];
}

#pragma mark -- Interstitials

- (void) showFullScreenAd
{
//    if(fullScreenAdType == FullScreen_ChartBoost)
//    {
//        [Chartboost showInterstitial:CBLocationHomeScreen];
//    }
//    else if(fullScreenAdType == FullScreen_AdColony)
//    {
//        [AdColony playVideoAdForZone:[[BREnvironment sharedEnvironment] stringForKey:@"AdColonyZone"] withDelegate:nil];
//    }
//    else if(fullScreenAdType == FullScreen_Both)
//    {
//        [Chartboost showInterstitial:CBLocationHomeScreen];
//        [AdColony playVideoAdForZone:[[BREnvironment sharedEnvironment] stringForKey:@"AdColonyZone"] withDelegate:nil];
//    }
}

#pragma mark -- Private

- (void) beginLoadingRulesForVerID:(int)versionID
{
    [[GPAdController sharedController] getAllRulesForVersion:[NSString stringWithFormat:@"%i", versionID] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSData *responseData = (NSData *)responseObject;
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                             options:kNilOptions
                                                               error:&error];
        
        if(error == nil)
        {
            if([[json objectForKey:@"Status"] intValue] == 201)
            {
                NSArray *rules = [[NSArray alloc]initWithArray:[json objectForKey:@"Data"]];
                
                if(rules != nil)
                {
                    for(NSDictionary *dict in rules)
                    {
                        RuleModel *rule = [[RuleModel alloc] init];
                        [rule mts_setValuesForKeysWithDictionary:dict];
                        
                        if([rule.status isEqualToString:@"ON"])
                        {
                            if([rule.ruleName isEqualToString:@"showBanner"])
                            {
                                for(RuleParamModel *param in rule.ruleParams)
                                {
                                    if([param.paramType isEqualToString:@"showADMOB"] && [param.paramValue boolValue] == YES)
                                    {
                                        //TODO: AdmobKey
//                                        [[LARSAdController sharedManager] registerAdClass:[TOLAdAdapterGoogleAds class] withPublisherId:[[BREnvironment sharedEnvironment] stringForKey:@"AdMobKey"]];
                                    }
                                    else if([param.paramType isEqualToString:@"showIAD"] && [param.paramValue boolValue] == YES)
                                    {
                                        [[LARSAdController sharedManager] registerAdClass:[TOLAdAdapteriAds class]];
                                    }
                                    else if([param.paramType isEqualToString:@"showAdcolony"])
                                    {
                                        //TODO:Ad colony key
//                                        [AdColony configureWithAppID:[[BREnvironment sharedEnvironment] stringForKey:@"AdColonyKey"]
//                                                             zoneIDs:@[[[BREnvironment sharedEnvironment] stringForKey:@"AdColonyZone"]]
//                                                            delegate:nil
//                                                             logging:NO];
                                        self.fullscreenAdTrigger = param.paramValue ? [param.paramValue intValue]: 0;
                                        
                                        if(fullScreenAdType == FullScreen_ChartBoost)
                                            fullScreenAdType = FullScreen_Both;
                                        else if(fullScreenAdType == FullScreen_None)
                                            fullScreenAdType = FullScreen_AdColony;
                                    }
                                    else if([param.paramType isEqualToString:@"showChartboost"])
                                    {
                                        //TODO: chartboost key
//                                        [Chartboost startWithAppId:[[BREnvironment sharedEnvironment] stringForKey:@"ChartboostKey"]
//                                                      appSignature:[[BREnvironment sharedEnvironment] stringForKey:@"ChartboostAppSignature"]
//                                                          delegate:nil];
                                        
                                        self.fullscreenAdTrigger = param.paramValue ? [param.paramValue intValue]: 0;
                                        if(fullScreenAdType == FullScreen_AdColony)
                                            fullScreenAdType = FullScreen_Both;
                                        else if(fullScreenAdType == FullScreen_None)
                                            fullScreenAdType = FullScreen_ChartBoost;
                                    }
                                    else if([param.paramType isEqualToString:@"defaultPromotionTrigger"])
                                    {
                                        [[TMCache sharedCache] setObject:[NSNumber numberWithInt:[param.paramValue intValue]] forKey:@"PromotionTrigger"];
                                    }
                                    else if([param.paramType isEqualToString:@"rateTrigger"])
                                    {
                                        [iRate sharedInstance].usesUntilPrompt = param.paramValue ? [param.paramValue integerValue]: 10;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        
            [[LARSAdController sharedManager] addAdContainerToViewInViewController:self];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"ADS - getAllRulesForVersion [Error: %@]", error.description);
    }];
}

#pragma mark -- iRate
- (void)iRateCouldNotConnectToAppStore:(NSError *)error
{
    NSLog(@"iRate Error: %@", error.description);
}

- (void)iRateDidPromptForRating
{
    NSLog(@"iRateDidPromptRating");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
