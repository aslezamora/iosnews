//
//  AFHTTPRequestOperation+Delegate.m
//  VoloProject
//
//  Created by Patrick Joseph Gorospe on 2/25/14.
//  Copyright (c) 2014 Volo International, Inc. All rights reserved.
//

#import "GPRequestOperationHelper.h"

@implementation GPRequestOperationHelper
{
    AFHTTPRequestOperationManager *sharedManager;
}
static GPRequestOperationHelper* _sharedHelper = nil;

+ (GPRequestOperationHelper*) sharedHelper
{
    if(_sharedHelper == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _sharedHelper = [[GPRequestOperationHelper alloc] init];
        });
    }
    return _sharedHelper;
}

-(id) init
{
    self = [super init];
    if (self) {
        sharedManager = [AFHTTPRequestOperationManager manager];
    }
    return self;
}

- (void)setRequestSerializer:(AFHTTPRequestSerializer <AFURLRequestSerialization> *)requestSerializer {
    NSParameterAssert(requestSerializer);
    
    [sharedManager setRequestSerializer:requestSerializer];
}

- (void)setResponseSerializer:(AFHTTPResponseSerializer <AFURLResponseSerialization> *)responseSerializer {
    NSParameterAssert(responseSerializer);
    
    [sharedManager setResponseSerializer:responseSerializer];
}

- (void) doPOST:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void (^)(AFHTTPRequestOperation *, id))success failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure
{
    [sharedManager POST:URLString parameters:parameters success:success failure:failure];
}

- (void)doPOST:(NSString *)URLString parameters:(NSDictionary *)parameters withDelegate:(id)delegate
{
    if(delegate == nil)
        return;
    
    _requestDelegate = delegate;
    
    [sharedManager POST:URLString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(_requestDelegate)
        {
            NSDictionary *response = (NSDictionary *) responseObject;
            
            if([_requestDelegate respondsToSelector:@selector(requestDidFinish:)])
                [_requestDelegate requestDidFinish:response];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(_requestDelegate)
        {
            if([_requestDelegate respondsToSelector:@selector(requestDidFail:)])
                [_requestDelegate requestDidFail:error];
        }
    }];
    
}

- (void) doGET:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void (^)(AFHTTPRequestOperation *, id))success failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure
{
    [sharedManager GET:URLString parameters:parameters success:success failure:failure];
}

- (void)doGET:(NSString *)URLString parameters:(NSDictionary *)parameters withDelegate:(id)delegate
{
    if(delegate == nil)
        return;
    
    _requestDelegate = delegate;
    
    [sharedManager GET:URLString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(_requestDelegate)
        {
            if([_requestDelegate respondsToSelector:@selector(requestDidFinish:)])
                [_requestDelegate requestDidFinish:responseObject];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if(_requestDelegate)
        {
            if([_requestDelegate respondsToSelector:@selector(requestDidFail:)])
                [_requestDelegate requestDidFail:error];
        }
    }];
    
}

- (NSOperationQueue *) sharedQueue
{
    return [sharedManager operationQueue];
}

@end
