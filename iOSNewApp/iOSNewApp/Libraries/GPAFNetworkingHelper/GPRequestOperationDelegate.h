//
//  GPRequestOperationDelegate.h
//  VoloProject
//
//  Created by Patrick Joseph Gorospe on 2/25/14.
//  Copyright (c) 2014 Volo International, Inc. All rights reserved.
//
#import "AFHTTPRequestOperationManager.h"
@class AFHTTPRequestOperation;

@protocol GPRequestOperationDelegate <NSObject>

@required
- (void)requestDidFinish:(id) responseObject;
- (void)requestDidFail:(NSError *) error;

@end
