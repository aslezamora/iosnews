//
//  AFHTTPRequestSerializerTimout.h
//  VoloProject
//
//  Created by Patrick Joseph Gorospe on 2/25/14.
//  Copyright (c) 2014 Volo International, Inc. All rights reserved.
//

#import "AFURLRequestSerialization.h"

static NSTimeInterval const kRequestTimoutInterval = 30.0;
@interface AFHTTPRequestSerializerTimout : AFHTTPRequestSerializer

@property (nonatomic, assign) NSTimeInterval timeout;

- (id)initWithTimeout:(NSTimeInterval)timeout;

@end
