//
//  AFHTTPRequestOperation+Delegate.h
//  VoloProject
//
//  Created by Patrick Joseph Gorospe on 2/25/14.
//  Copyright (c) 2014 Volo International, Inc. All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "GPRequestOperationDelegate.h"

@interface GPRequestOperationHelper : NSObject
{
    
}

@property (nonatomic, weak) id<GPRequestOperationDelegate> requestDelegate;


+ (GPRequestOperationHelper *) sharedHelper;

- (void)setRequestSerializer:(AFHTTPRequestSerializer <AFURLRequestSerialization> *)requestSerializer;
- (void)setResponseSerializer:(AFHTTPResponseSerializer <AFURLResponseSerialization> *)responseSerializer;
- (void)doPOST:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
       failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
- (void)doPOST:(NSString *)URLString parameters:(NSDictionary *)parameters withDelegate:(id)delegate;

- (void) doGET:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void (^)(AFHTTPRequestOperation *, id))success failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure;

- (void)doGET:(NSString *)URLString parameters:(NSDictionary *)parameters withDelegate:(id)delegate;

- (NSOperationQueue *)sharedQueue;
@end
