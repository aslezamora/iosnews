//
//  AFHTTPRequestSerializerTimout.m
//  VoloProject
//
//  Created by Patrick Joseph Gorospe on 2/25/14.
//  Copyright (c) 2014 Volo International, Inc. All rights reserved.
//

#import "AFHTTPRequestSerializerTimout.h"

@implementation AFHTTPRequestSerializerTimout

- (id)initWithTimeout:(NSTimeInterval)timeout
{
    self = [super init];
    if (self) {
        self.timeout = timeout;
    }
    return self;
}

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method
                                 URLString:(NSString *)URLString
                                parameters:(NSDictionary *)parameters
                                     error:(NSError *__autoreleasing *)error
{
    NSMutableURLRequest *request = [super requestWithMethod:method URLString:URLString parameters:parameters error:error];
    
    if (self.timeout > 0) {
        [request setTimeoutInterval:self.timeout];
    }
    return request;
}
@end
