//
//  RuleModel.h
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 1/6/15.
//  Copyright (c) 2015 SurrealFacts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Motis.h>
@interface RuleParamModel : NSObject

@property (nonatomic) int ID;
@property (nonatomic, strong) NSString *ruleID;
@property (nonatomic, strong) NSString *paramType;
@property (nonatomic, strong) NSString *paramValue;
@end
