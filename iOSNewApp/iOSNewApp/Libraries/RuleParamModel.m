//
//  RuleModel.m
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 1/6/15.
//  Copyright (c) 2015 SurrealFacts. All rights reserved.
//

#import "RuleParamModel.h"

@implementation RuleParamModel

+ (NSDictionary*)mts_mapping
{
    return @{@"id": mts_key(ID),
             @"rule_id": mts_key(ruleID),
             @"param_type": mts_key(paramType),
             @"param_value": mts_key(paramValue),
             };
}

+ (BOOL) mts_shouldSetUndefinedKeys
{
    return YES;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
