//
//  AdsController.h
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 12/19/14.
//  Copyright (c) 2014 SurrealFacts. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    iOS,
    Android
} Platform;

typedef NS_ENUM(NSInteger, GPAdNetwork) {
    GPAdNetworkiAd = 1,        ///< iAd
    GPAdNetworkAdMob           ///< AdMob
};

typedef NS_ENUM(NSInteger, GPPAdPosition) {
    GPAdPositionBottom = 1,    ///< Ads positioned at the bottom of your view controller.
    GPAdPositionTop            ///< Ads positioned at the top of your view controller.
};

@class AFHTTPRequestOperation;

@interface GPAdController : UIViewController

+ (GPAdController *) sharedController;

- (void)getAppVersionsWithSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)getAppPromotionsWithSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)getAppDefaultPromotionsForPlatform:(Platform)platform success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)getAllRulesForVersion:(NSString *)version success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)getActiveRuleForVersion:(NSString *)version success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

@end
