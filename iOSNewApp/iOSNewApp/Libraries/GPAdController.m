//
//  AdsController.m
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 12/19/14.
//  Copyright (c) 2014 SurrealFacts. All rights reserved.
//

#import "GPAdController.h"
#import "GPRequestOperationHelper.h"
#import <AdSupport/AdSupport.h>
#import "AFHTTPRequestSerializerTimout.h"

static NSString *const EndpointGetAppVersions = @"/getAppVersionsWithKey";
static NSString *const EndpointGetAppPromotion = @"/getAppPromotionWithKey";
static NSString *const EndpointGetAppDefaultPromotion = @"/getAppDefaultPromotionsWithPlatformType";
static NSString *const EndpointGetAllRules = @"/getAppAllRulesWithAppKeyAndVerID";
static NSString *const EndpointGetAppActiveRule = @"/getAppActiveRuleWithAppKeyAndVerID";

@interface GPAdController ()

@end

@implementation GPAdController
{
    NSString *adsURL;
    NSString *adsAPIKey;
    
    GPRequestOperationHelper *requestHelper;
}

static GPAdController *_sharedController = nil;

+ (GPAdController *) sharedController
{
    if(_sharedController == nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _sharedController = [[GPAdController alloc] init];
        });
    }
    return _sharedController;
}

- (id) init
{
    self = [super init];
    if (self) {
        adsURL = @"http://adsystem.gotdns.com/api";
        adsAPIKey = @"JPcoQZLEBoE3IeEAIqiaUykwHnQctkmw";
        requestHelper = [[GPRequestOperationHelper alloc] init];
        [requestHelper setRequestDelegate:nil];
        [requestHelper setRequestSerializer:[[AFHTTPRequestSerializerTimout alloc] initWithTimeout:kRequestTimoutInterval]];
        [requestHelper setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    }
    
    return self;
}

- (void)getAppVersionsWithSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSDictionary *parameters = @{
                                 @"app_key": adsAPIKey,
                                 };
    
    NSString *url = [NSString stringWithFormat:@"%@%@", adsURL, EndpointGetAppVersions];
    [requestHelper doPOST:url parameters:parameters success:success failure:failure];
}

- (void)getAppPromotionsWithSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSDictionary *parameters = @{
                                 @"app_key": adsAPIKey,
                                 };
    
    NSString *url = [NSString stringWithFormat:@"%@%@", adsURL, EndpointGetAppPromotion];
    [requestHelper doPOST:url parameters:parameters success:success failure:failure];
}

- (void)getAppDefaultPromotionsForPlatform:(Platform)platform success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSDictionary *parameters = @{
                                 @"platform_type": [self convertPlatformToString:platform],
                                 };
    
    NSString *url = [NSString stringWithFormat:@"%@%@", adsURL, EndpointGetAppDefaultPromotion];
    [requestHelper doPOST:url parameters:parameters success:success failure:failure];
}

- (void)getAllRulesForVersion:(NSString *)version success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSDictionary *parameters = @{
                                 @"app_key": adsAPIKey,
                                 @"ver_id": version
                                 };
    
    NSString *url = [NSString stringWithFormat:@"%@%@", adsURL, EndpointGetAllRules];
    [requestHelper doPOST:url parameters:parameters success:success failure:failure];
}

- (void)getActiveRuleForVersion:(NSString *)version success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSDictionary *parameters = @{
                                 @"app_key": adsAPIKey,
                                 @"ver_id": version
                                 };
    
    NSString *url = [NSString stringWithFormat:@"%@%@", adsURL, EndpointGetAppActiveRule];
    [requestHelper doPOST:url parameters:parameters success:success failure:failure];
}

#pragma mark -- Private
- (NSString *) convertPlatformToString:(Platform) platform {
    NSString *result = nil;
    
    switch(platform) {
        case iOS:
            result = @"ios"; 
            break;
        case Android:
            result = @"android";
            break;
            
        default:
            result = @"ios";
    }
    
    return result;
}
@end
