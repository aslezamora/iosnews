//
//  AppInfoModel.h
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 1/6/15.
//  Copyright (c) 2015 SurrealFacts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Motis.h>
@interface AppInfoModel : NSObject

@property (nonatomic) int ID;
@property (nonatomic) int appID;
@property (nonatomic, strong) NSString *appDescription;
@property (nonatomic, strong) NSString *versionNumber;

@end
