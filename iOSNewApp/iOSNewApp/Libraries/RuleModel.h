//
//  RuleModel.h
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 1/6/15.
//  Copyright (c) 2015 SurrealFacts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Motis.h>

#import "RuleParamModel.h"
@interface RuleModel : NSObject

@property (nonatomic, strong) NSString *ruleName;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSArray *ruleParams;

@end
