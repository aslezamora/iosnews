//
//  AppInfoModel.m
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 1/6/15.
//  Copyright (c) 2015 SurrealFacts. All rights reserved.
//

#import "AppInfoModel.h"

@implementation AppInfoModel

+ (NSDictionary*)mts_mapping
{
    return @{@"id": mts_key(ID),
             @"app_id": mts_key(appID),
             @"description": mts_key(appDescription),
             @"ver_number": mts_key(versionNumber),
             };
}

+ (BOOL) mts_shouldSetUndefinedKeys
{
    return YES;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
