//
//  RuleModel.m
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 1/6/15.
//  Copyright (c) 2015 SurrealFacts. All rights reserved.
//

#import "RuleModel.h"

@implementation RuleModel

+ (NSDictionary*)mts_mapping
{
    return @{@"rule_name": mts_key(ruleName),
             @"status": mts_key(status),
             @"rule_params": mts_key(ruleParams),
             };
}

+ (NSDictionary*)mts_arrayClassMapping
{
    return @{mts_key(ruleParams) : [RuleParamModel class],};
}

+ (BOOL) mts_shouldSetUndefinedKeys
{
    return YES;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

@end
