//
//  SFViewController.h
//  SurrealFactsApp
//
//  Created by Patrick Gorospe on 12/19/14.
//  Copyright (c) 2014 SurrealFacts. All rights reserved.
//

#import "TOLAdAdapterGoogleAds.h"
#import "TOLAdAdapteriAds.h"
#import <LARSAdController/LARSAdController.h>

@interface SFViewController : UIViewController

@property (nonatomic) int fullscreenAdTrigger;

- (void) showFullScreenAd;
@end
