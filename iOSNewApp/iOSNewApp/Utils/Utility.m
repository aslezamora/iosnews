//
//  Utility.m
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/13/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import "Utility.h"
#import <UIKit/UIStringDrawing.h>

#define SYSTEM_VERSION_LESS_THAN(v)    ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@implementation Utility

+ (CGFloat) getTextHeight:(NSString*)str atFont:(UIFont*)font
{
//    CGSize size = CGSizeMake(280,300);
//    CGRect textRect = [str
//                       boundingRectWithSize:size
//                       options:NSStringDrawingUsesLineFragmentOrigin
//                       attributes:@{NSFontAttributeName:font}
//                       context:nil];
//    return textRect.size.height;
    CGSize size = CGSizeMake(280,300);
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")){
        CGSize textSize = [str sizeWithFont:font constrainedToSize:size];
        return textSize.height;
    }else{
        CGRect textRect = [str
                           boundingRectWithSize:size
                           options:NSStringDrawingUsesLineFragmentOrigin
                           attributes:@{NSFontAttributeName:font}
                           context:nil];
        return textRect.size.height;
    }
}

+ (void) alignTextBottom:(NSString*)str label:(UILabel *)label font:(UIFont *)font
{
    CGSize size = CGSizeMake(280,300);
    CGRect textRect = [str boundingRectWithSize:size
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSFontAttributeName:font}
                                        context:nil];
    
    CGSize textSize = textRect.size;
    
    if (textSize.height>17) {
        
        CGRect newRect = label.frame;
        newRect.origin.y += (newRect.size.height - textSize.height);
        newRect.size.height = textSize.height;
        label.frame = newRect;
    }
    else {
        
        CGRect newRect = label.frame;
        newRect.origin.y = (newRect.size.height - textSize.height);
        newRect.size.height = textSize.height;
        label.frame = newRect;
    }
    
}

+(NSString *) getPostDateDifference:(NSDate *)postDate{
    NSString *strPostedDateDiff;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    
    NSString *dateInString = [dateFormatter stringFromDate:postDate];
    NSString *monthString = [dateInString substringWithRange:NSMakeRange(5, 2)];
    NSString *dayString = [dateInString substringWithRange:NSMakeRange(8, 2)];
    NSString *month_Key = [self getMonthShort:monthString];
    
    NSString *monthDay = [NSString stringWithFormat:@"%@ %@", month_Key, dayString];
    if (dateInString != nil) {
        //NSDate *postDate = [dateFormatter dateFromString:dateInString];
        NSTimeInterval timeDifference = [[NSDate date] timeIntervalSinceDate:postDate];
        long difference =  timeDifference ;
        
        int seconds = (int) (difference % 60);
        
        difference /= 60;
        int minutes = (int) (difference % 60);
        
        difference /= 60;
        int hours = (int) (difference % 24);
        
        difference /= 24;
        int days = (int) difference;
        
        if (days > 0 && days <= 10) {
            if (days == 1)
                strPostedDateDiff = @"A day ago";
            else
                strPostedDateDiff = [NSString stringWithFormat:@"%d days ago", days];
        } else if (days <= 0 && hours > 0) {
            if (hours == 1)
                strPostedDateDiff = @"An hour ago";
            else
                strPostedDateDiff = [NSString stringWithFormat:@"%d hrs ago",hours];
        } else if (days <= 0 && hours <= 0 && minutes > 0) {
            if (minutes == 1)
                strPostedDateDiff = @"A minute ago";
            else
                strPostedDateDiff = [NSString stringWithFormat:@"%d mins ago",minutes];
        } else if (days <= 0 && hours <= 0 && minutes <= 0 && seconds > 0) {
            strPostedDateDiff = [NSString stringWithFormat:@"%d seconds ago", seconds];
        } else if (days > 10) {
            strPostedDateDiff = [NSString stringWithFormat:@"On %@", monthDay];
        } else {
            strPostedDateDiff = @"No value";
        }
        return strPostedDateDiff;
    }
    return @"";
}

+(NSString *)getMonthShort:(NSString *)month_Key {
    
    if ([month_Key isEqualToString:@"01"])
        month_Key = @"Jan";
    else if ([month_Key isEqualToString: @"02"])
        month_Key = @"Feb";
    else if ([month_Key isEqualToString:@"03"])
        month_Key = @"Mar";
    else if ([month_Key isEqualToString:@"04"])
        month_Key = @"Apr";
    else if ([month_Key isEqualToString:@"05"])
        month_Key = @"May";
    else if ([month_Key isEqualToString:@"06"])
        month_Key = @"Jun";
    else if ([month_Key isEqualToString:@"07"])
        month_Key = @"Jul";
    else if ([month_Key isEqualToString:@"08"])
        month_Key = @"Aug";
    else if ([month_Key isEqualToString:@"09"])
        month_Key = @"Sep";
    else if ([month_Key isEqualToString:@"10"])
        month_Key = @"Oct";
    else if ([month_Key isEqualToString: @"11"])
        month_Key = @"Nov";
    else if ([month_Key isEqualToString:@"12"])
        month_Key = @"Dec";
    
    return month_Key;
}

@end
