//
//  Constant.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 3/13/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <Foundation/Foundation.h>

#define font_sideMenu                   @"Utsaah"
#define font_secondView_Regular			@"Lato-Regular"
#define font_secondView_Bold			@"Lato-Bold"

@interface Constant : NSObject

@end
