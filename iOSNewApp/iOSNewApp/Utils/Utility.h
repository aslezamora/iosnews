//
//  Utility.h
//  iOSNewApp
//
//  Created by cpccqo143461 on 2/13/15.
//  Copyright (c) 2015 LAZ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utility : NSObject

+ (CGFloat) getTextHeight:(NSString*)str atFont:(UIFont*)font;
+ (void) alignTextBottom:(NSString*)str label:(UILabel *)label font:(UIFont *)font;
+(NSString *) getPostDateDifference:(NSDate *)dateInString;
+(NSString *)getMonthShort:(NSString *)month_Key;

@end
